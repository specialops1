<?php
$require_login = true;
$page_name = 'Message Detail';
require 'config.php';
$level_restriction = NEW_USER;
require 'top.inc.php';

if ( empty($_GET['m']) )
	printf('Invalid message ID.') && footer();

$minfo = mysql_fetch_assoc(mysql_query('SELECT
`messages`.`user`, `messages`.`topic`, `messages`.`message`, `messages`.`visible`, UNIX_TIMESTAMP(`messages`.`time`) AS `time`,
`message-data`.`content`,
`topics`.`board`, `topics`.`title`,
`boards`.`name`
FROM `messages`
NATURAL LEFT JOIN `message-data`
NATURAL LEFT JOIN `topics`
NATURAL LEFT JOIN `boards`
WHERE `messages`.`message` = '.intval($_GET['m'])));

if ( !$minfo )
	printf('Invalid message ID.') && footer();

/*** form strings ***/
// mark/suggest others' posts
$form = array(
'mark' => '<div class="c3">Mark Message</div>
<div class="c2">You can use this form to mark posts for moderation or suggest them for '.$strings['cookies'].'. Just put a reason in the box and click the button.</div>
<div class="c1">
<input type="text" name="markreason"/><br/>
<input type="submit" name="submit" value="Mark"/>
<input type="submit" name="submit" value="Suggest"/>
</div>',

'deletemsg' => '<div class="c3">Delete Message</div>
<div class="c1"><input type="submit" name="submit" value="Delete Message"/></div>',

'deletetopic' => '<div class="c3">Delete Topic</div>
<div class="c1"><input type="submit" name="submit" value="Delete Topic"/></div>',

'close' => '<div class="c3">Close Topic</div>
<div class="c1"><input type="submit" name="submit" value="Close Topic"/></div>',

'no' => '<div class="c3">No Action Availiable</div>',

'mod' =>  '<div class="c3">Message Action</div>
<div class="c2">
<input type="radio" name="action" value="none" checked="checked"/> Do nothing or
<input type="text" size="3" name="aura" value="0"/> '.$strings['cookies'].'<br/>
<input type="radio" name="action" value="post"/> Delete post<br/>
<input type="radio" name="action" value="topic"/> Delete topic<br/>
<input type="radio" name="action" value="close"/> Close topic<br/>
<input type="radio" name="action" value="suspend"/> Suspend user<br/>
<input type="submit" name="submit2" value="Confirm"/>
</div>'
);

// the bit it does when the form gets submitted
if ( isset($_POST['submit']) ) {

	if ( $_POST['submit'] == 'Mark' ) {}
	elseif ( $_POST['submit'] == 'Suggest' ) {}
	elseif ( $_POST['submit'] == 'Delete Message' && $minfo['user'] == $userinfo['user'] ) {
		mysql_query('UPDATE `messages` SET `visible` = -1 WHERE `message` = '.$minfo['message'])
			&& printf('Message deleted.') && footer();
	}
	elseif ($_POST['submit'] == 'Delete Topic') {}
	elseif ($_POST['submit'] == 'Close Topic') {}
}

if ( isset($_POST['submit2']) && $userinfo['level'] >= MOD ) {
	if ( $_POST['action'] == 'close' ) {}
	elseif ( $_POST['action'] == 'post' ) {}
	elseif ( $_POST['action'] == 'topic' ) {}
	elseif ( $_POST['action'] == 'suspend' ) {}
	elseif ( $_POST['action'] == 'none' ) {}
	printf($msg.' Return to the <a href="markqueue'.URL_STRING.'">message queue</a>.') && footer();
}

echo '<div class="c3">Board: <a href="viewboard?b=',$minfo['board'],'">',$minfo['name'],'</a> |
Topic: <a href="viewtopic?t=',$minfo['topic'],'">',$minfo['title'],'</a></div>

<div class="c2">From: ',userlink($minfo['user']),' | Posted: ',date2($minfo['time']),'</div>

<div class="c1">',"\n";

if ( $userinfo['level'] >= MOD || $minfo['visible'] >= 0 )
	echo $minfo['content'];
elseif ( $minfo['visible'] = -1 )
	echo '[Message has been deleted by the poster]';
elseif ( $minfo['visible'] = -2 )
	echo '[Message has been deleted by a moderator]';
echo '</div>

<form method="post" action="',$_SERVER['PHP_SELF'],'?m=',$minfo['message'],URL_APPEND,"\">\n";

if ( $minfo['user'] == $userinfo['user'] ) {
	$postnum = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages`
			WHERE `topic` = '.$minfo['topic'].' AND `message` < '.$minfo['message']), 0);
	$postnum2 = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages`
			WHERE `topic` = '.$minfo['topic'].' AND `message` > '.$minfo['message']), 0);
	if ( $postnum )
		echo $form['deletemsg'];
	elseif ( !$postnum2 )
		echo $form['deletetopic'];
	elseif ( $postnum2 && mysql_result(mysql_query('SELECT UNIX_TIMESTAMP(`time`) FROM `messages`
			WHERE `topic` = '.$minfo['topic'].' AND `message` > '.$minfo['message'].' ORDER BY `time` DESC'), 0) < time() - 1200 )
		echo $form['close'];
	else
		echo $form['no'];
}
else
	echo $form['mark'];
if ( $userinfo['level'] >= MOD )
	echo $form['mod'];
echo '</form>';

footer();
?>
