<?php
$require_login = true;
$page_name = 'Marked Messages';
require 'config.php';
$level_restriction = MOD;
require 'top.inc.php';

$lastmod = mysql_fetch_row (mysql_query ('SELECT `lastmod`, `lastmodat` FROM `misc2`'));
echo '<div class="c3">Last Mod: '.userlink ($lastmod[0]).' at '.date2 ($lastmod[1]).'</div>
';
mysql_query ('UPDATE `misc2` SET `lastmod` = \''.$userinfo['userid'].'\', `lastmodat` = \''.time().'\'');

function queuedraw ($type, $verb)
{
	echo '<div class="c3">'.$verb.' Messages</div>
<table>
<tr>
<th>Mark ID</th>
<th>Message ID</th>
<th>'.$verb.' By</th>
<th>'.$verb.' At</th>
<th>'.$verb.' Times</th>
<th>User '.$verb.'</th>
<th>Board</th>
<th>Topic</th>
<th>Reason</th>
</tr>
';
	$result = mysql_query ('SELECT `markid`, `marks`.`msgid`, `by`, `at`, COUNT(*) AS `times`, `postedby`, `mesboard`, `topic`, `reason`
FROM `marks`, `msgmeta`
WHERE `modat` = 0 AND `marks`.`msgid` = `msgmeta`.`msgid` AND `type` = \''.$type.'\'
GROUP BY `marks`.`msgid`
ORDER BY `times` DESC
LIMIT 30');
	while ($myrow = mysql_fetch_row ($result))
	{
		$titles = mysql_fetch_row (mysql_query ('SELECT `boardname`, `topicname` FROM `boards`, `topics` WHERE `boardid` = \''.$myrow[6].'\' AND `topicid` = \''.$myrow[7].'\''));
		echo '<tr class='.colour().'>
<td>'.$myrow[0].'</td>
<td><a href="message.php?m='.$myrow[1].urlpath(1).'">'.$myrow[1].'</a></td>
<td>'.userlink ($myrow[2]).'</td>
<td>'.date2 ($myrow[3]).'</td>
<td>'.$myrow[4].'</td>
<td>'.userlink ($myrow[5]).'</td>
<td><a href="board.php?b='.$myrow[6].'">'.$titles[0].'</a></td>
<td><a href="topic.php?b='.$myrow[6].';t='.$myrow[7].'">'.$titles[1].'</a></td>
<td>'.$myrow[8].'</td>
</tr>
';
	}
echo '</table>';
	return 1;
}
if (mysql_result (mysql_query ('SELECT COUNT(*) FROM `marks` WHERE `modat` = 0 AND `type` = 2'), 0))
	$queue = queuedraw ('suggest', 'Suggested');
if (mysql_result (mysql_query ('SELECT COUNT(*) FROM `marks` WHERE `modat` = 0 AND `type` = 1'), 0))
	$queue = queuedraw ('mark', 'Marked');
if (!isset ($queue))
	echo '<div class="alert">Queue is currently empty.</div>';

require ('foot.php');
?>