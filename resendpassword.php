<?php
require ('config.php');
$page_name = 'Lost Password';
require ('top.inc.php');
require ('encryption.inc.php');

echo '<div class="alert">';

if (mysql_num_rows (mysql_query ('SELECT `id` FROM `iplog` WHERE `actiontype` = 4 AND `remoteip` = \''.$_SERVER['REMOTE_ADDR'].'\' AND `logsec` > '.(time()-86400))) > 2)
	echo 'To prevent email flooding you can only resend a password three times a day. Try again later.';
elseif (isset ($_POST['submit']))
{
	if (!isset ($_POST['uname']))
		echo 'Enter your username.';
	elseif (empty ($_POST['email']))
		echo 'Enter your email address.';
	elseif (!mysql_result (mysql_query ('SELECT COUNT(*) FROM `users` WHERE
`username` = \''.mysql_real_escape_string ($_POST['uname']).'\' AND
`email` = \''.mysql_real_escape_string ($_POST['email']).'\''), 0))
		echo 'No user with that name and email address found.';
	else
	{
		$pass = mysql_fetch_row (mysql_query ('SELECT `password`, `regip` FROM `users` WHERE
`username` = \''.mysql_real_escape_string ($_POST['uname']).'\' AND
`email` = \''.mysql_real_escape_string ($_POST['email']).'\' LIMIT 1'));
		mysql_query ('INSERT INTO `iplog` (`logsec`, `actiontype`, `remoteip`) VALUES (\''.time().'\', 4, \''.$_SERVER['REMOTE_ADDR'].'\')');
		$userid = mysql_insert_id($db);
		$mail_body = 'Password for user at '.$cfg['bds']['name'].':
	'.decrypt ($pass[0], $pass[1]).'
This request was made from the IP '.$_SERVER['REMOTE_ADDR'].' at '.date2(time());
		if (mail ($_POST['email'], 'Lost Password', $mail_body))
			echo 'E-mail sent. Return to the <a href="login.php">login page</a>.';
		else
			echo 'Can\'t send email. Go and harass one of the admins to change your pass or something.';
	}
}

echo '</div>

<div class="c3">Resend Password</div>

<div class='.colour().'>
<form method="post" action="'.urlpath(2).'">
Enter your username and the email you entered in the "Private Email Address" field to have your password emailed to you.<br/>
Username:<br/>
<input type="text" name="uname" /><br/>
Email:<br/>
<input type="text" name="email" /><br/>
<input type="submit" name="submit" value="Send"/>
</form>
</div>';

require ('foot.php');
?>