<?php
require 'config.php';
require 'include/class.message.php';
$require_login = true;
$page_name = isset($_GET['t']) ? 'Post Message' : 'Create Topic';

$boardinfo = mysql_fetch_row(mysql_query('SELECT `post_level`, `topic_level`, `board` FROM `boards`
		WHERE `board` = '.intval($_GET['b'])));

$level_restriction = isset ($_GET['t']) ?  $boardinfo[0] : $boardinfo[1];
$topic_level = $boardinfo[1];
require 'top.inc.php';

// Board/topic checking
if ( empty($boardinfo[2]) || !is_numeric($_GET['b']) || (isset($_GET['t']) && !is_numeric($_GET['t'])) )
	stop('Invalid link.');

elseif ( isset($_GET['t']) &&
!mysql_result(mysql_query('SELECT COUNT(*) FROM `topics` WHERE `topic` = '.intval($_GET['t']).' AND `board` = '.intval($_GET['b'])), 0) )
	stop('Invalid topic link.');

elseif ( isset($_GET['t']) &&
mysql_result(mysql_query('SELECT `visible` FROM `topics` WHERE `topic` = '.intval($_GET['t'])), 0) < 0 )
	stop('<div class="alert">This topic has been closed or deleted. You cannot post in it.</div>');

// Rate limits
$msglimit = 180 / $userinfo['level'];
$tpclimit = 90 - $userinfo['level'];

if ( isset($_POST['submit']) ) {
	// Strip crap from topic title
	$topic_title = trim($_POST['topictitle']);
	if ( $userinfo['level'] >= ADMIN && $_POST['tophtm'] )
		$html_title = $topic_title;
	elseif ( $userinfo['level'] >= REG_USER && $_POST['tophtm'] )
		$html_title = '<em>'.htmlspecialchars($topic_title).'</em>';
	else
		$html_title = htmlspecialchars($topic_title);

	$user_message = new Message($_POST['msgtext']);
	if ( $user_message->output !== false )
		$html_message = $user_message->output;

	// Error checks
	if ( empty($_GET['t']) && $topic_title === '' )
		echo '<div class="alert">Topic titles can\'t be blank.</div>';
	elseif ( trim(preg_replace('#<[a-z]+.*>#Usi', '', $html_message)) === '' )
		echo '<div class="alert">You can\'t post blank messages.</div>';
	elseif ( $user_message->output === false )
		echo '<div class="alert">You have an HTML error in your post somewhere.</div>';

	// Shows preview if there are no errors
	elseif ( $_POST['submit'] === 'Preview' ) {
		echo '<div class="alert">Preview Message</div>',"\n",
		( !empty($html_title) ? '<div class="c2">Topic: '.$html_title."</div>\n" : '' ).
		'<div class="c1">',$html_message,"</div>\n";
	}

	// The post bit
	elseif ( $_POST['submit'] ) {
		if ( empty($_GET['t']) ) {
			if ( mysql_result(mysql_query('SELECT COUNT(*) FROM `topics`
					WHERE `title` = \''.mysql_real_escape_string(strip_tags($html_title)).'\'
					AND `board` = '.intval($_GET['b'])), 0) >= 1)
				stop('<div class="alert">A topic with this title has already been posted on this board.</div>');
			mysql_query('INSERT INTO `topics` (`title`, `board`, `user`) VALUES (
					\''.mysql_real_escape_string($html_title).'\',
					'.intval($_GET['b']).',
					'.$userinfo['user'].')');
			$topicid = mysql_insert_id();
			$thingy = 'board';
		} else {
			$topicid = intval($_GET['t']);
			$thingy = 'topic';
		}
		mysql_query('INSERT INTO `message-data` (`content`) VALUES (\''.mysql_real_escape_string ($html_message).'\')');
		mysql_query('INSERT INTO `messages` (`message`, `topic`, `user`, `ip`, `time`) VALUES (
				LAST_INSERT_ID(),
				'.$topicid.',
				'.$userinfo['user'].',
				INET_ATON(\''.$_SERVER['REMOTE_ADDR'].'\'),
				NOW()
			)');

		if ( $userinfo['points'] <= 74 )
			mysql_query('UPDATE `users` SET `points` = (`points` + 1) WHERE `user` = '.$userinfo['user']);
		else
			mysql_query('UPDATE `users` SET `points` = 0, `cookies` = (`cookies` + 1) WHERE `user` = '.$userinfo['user']);

		header('HTTP/1.1 303 See Other');
		header('Location: view'.$thingy.'.php'.URL_STRING);

		stop('Return to from which you <a href="'.$thingy.'.php'.URL_STRING.'">came</a>.');
	}
}

$msgtext = isset($_POST['msgtext']) ? htmlspecialchars($_POST['msgtext']) : '';

if ( !$msgtext && $userinfo['sig'] )
	$msgtext = '
---
'.$userinfo['sig'];

echo '<div class="c3">Post Message</div>';

if ( empty($_GET['t']) ) {
	echo '<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,"\">\n",
		"<fieldset>\n",
		"<legend>Topic</legend>\n";

	echo 'Topic Title';
	if ( $userinfo['level'] >= ADMIN )
		echo '<small>(<label for="title_html">Enable HTML <input type="checkbox" name="tophtm"',
				( !empty($_POST['tophtm']) ? ' checked="checked"' : '' ),' accesskey="e"/></label>)</small>',"\n";
	echo '<br/>
<input type="text" maxlength="80" style="width: 100%; display: block"'.( isset($topic_title) ? ' value="'.htmlentities ($topic_title).'"' : '' ).' name="topictitle"/>
</fieldset>';
} else {
	list($topicname) = mysql_fetch_row(mysql_query('SELECT `title` FROM `topics` WHERE `topic` = '.intval($_GET['t'])));
	echo '<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,"\">\n",
		'<div class="c2">Topic: <a href="topic.php',URL_STRING,'">',$topicname,"</a></div>\n";
}

echo '<fieldset>
<legend>Message <small>(X)</small></legend>
<textarea style="width: 100%" rows="20" cols="80" name="msgtext" accesskey="x">',$msgtext,'</textarea>

<div>
<button type="submit" name="submit" value="post" accesskey="p"><u>P</u>ost</button>
<button type="submit" name="submit" value="Preview" accesskey="r">P<u>r</u>eview</button>
</div>
</fieldset>
</form>';

require 'foot.php';
?>
