-- phpMyAdmin SQL Dump
-- version 2.6.4-pl2
-- http://www.phpmyadmin.net
-- 
-- Host: minesweeper
-- Generation Time: Oct 22, 2005 at 06:39 PM
-- Server version: 4.1.14
-- PHP Version: 5.0.5
-- 
-- Database: `specialops`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `board-groups`
-- 

CREATE TABLE `board-groups` (
  `group` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(16) NOT NULL default '',
  PRIMARY KEY  (`group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `board-groups`
-- 

INSERT INTO `board-groups` VALUES (1, 'TEST PLZ');

-- --------------------------------------------------------

-- 
-- Table structure for table `boards`
-- 

CREATE TABLE `boards` (
  `board` tinyint(4) unsigned NOT NULL auto_increment,
  `group` tinyint(3) unsigned NOT NULL default '0',
  `name` varchar(85) NOT NULL default '',
  `caption` varchar(85) NOT NULL default '',
  `view_level` tinyint(3) unsigned NOT NULL default '0',
  `topic_level` tinyint(3) unsigned NOT NULL default '0',
  `post_level` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`board`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `boards`
-- 

INSERT INTO `boards` VALUES (1, 1, 'Goatse', '*too lazy to make other boards*', 0, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `marks`
-- 

CREATE TABLE `marks` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `msgid` mediumint(8) unsigned NOT NULL default '0',
  `user` smallint(5) unsigned NOT NULL default '0',
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `reason` char(85) NOT NULL default '',
  `actioned` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `action` tinyint(4) NOT NULL default '0',
  `moderator` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `mk_msgid` (`msgid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `marks`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `message-data`
-- 

CREATE TABLE `message-data` (
  `message` mediumint(8) unsigned NOT NULL auto_increment,
  `content` text NOT NULL,
  PRIMARY KEY  (`message`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `message-data`
-- 

INSERT INTO `message-data` VALUES (1, 'a bunny ._.');
INSERT INTO `message-data` VALUES (2, 'omg a post');

-- --------------------------------------------------------

-- 
-- Table structure for table `messages`
-- 

CREATE TABLE `messages` (
  `message` mediumint(8) unsigned NOT NULL auto_increment,
  `topic` mediumint(8) unsigned NOT NULL default '0',
  `user` smallint(5) unsigned NOT NULL default '0',
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `score` tinyint(3) NOT NULL default '0',
  `visible` tinyint(1) NOT NULL default '0',
  `ip` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`message`),
  KEY `topic` (`topic`),
  KEY `visible` (`visible`),
  KEY `score` (`score`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `messages`
-- 

INSERT INTO `messages` VALUES (1, 1, 1, '2005-08-10 23:22:38', 0, 0, 3232235522);
INSERT INTO `messages` VALUES (2, 1, 1, '2005-08-13 18:46:42', 0, -1, 3232235522);

-- --------------------------------------------------------

-- 
-- Table structure for table `styles`
-- 

CREATE TABLE `styles` (
  `style` tinyint(3) unsigned NOT NULL auto_increment,
  `title` char(6) NOT NULL default '',
  `file` char(6) NOT NULL default '',
  PRIMARY KEY  (`style`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `styles`
-- 

INSERT INTO `styles` VALUES (1, 'Default', 'default.css');

-- --------------------------------------------------------

-- 
-- Table structure for table `topics`
-- 

CREATE TABLE `topics` (
  `topic` mediumint(8) unsigned NOT NULL auto_increment,
  `title` varchar(85) NOT NULL default '',
  `user` smallint(5) unsigned NOT NULL default '0',
  `board` tinyint(4) unsigned NOT NULL default '0',
  `visible` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`topic`),
  KEY `board` (`board`),
  KEY `visible` (`visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `topics`
-- 

INSERT INTO `topics` VALUES (1, 'omg', 1, 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

CREATE TABLE `users` (
  `user` smallint(5) unsigned NOT NULL auto_increment,
  `level` tinyint(3) NOT NULL default '10',
  `name` varchar(9) NOT NULL default '',
  `password` varchar(85) character set utf8 collate utf8_bin NOT NULL default '',
  `points` tinyint(4) NOT NULL default '0',
  `cookies` smallint(6) NOT NULL default '0',
  `topics_page` tinyint(3) unsigned NOT NULL default '35',
  `msgs_page` tinyint(3) unsigned NOT NULL default '35',
  `timezone` tinyint(2) NOT NULL default '0',
  `theme` tinyint(3) unsigned NOT NULL default '1',
  `header` tinyint(3) unsigned NOT NULL default '0',
  `last_active` datetime NOT NULL default '0000-00-00 00:00:00',
  `register_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `sig` varchar(85) NOT NULL default '',
  `quote` varchar(85) NOT NULL default '',
  `useragent` varchar(100) NOT NULL default '',
  `public_email` varchar(20) NOT NULL default '',
  `private_email` varchar(20) NOT NULL default '',
  `register_email` varchar(20) NOT NULL default '',
  `last_ip` int(10) unsigned default NULL,
  `last_login_ip` int(10) unsigned default NULL,
  `register_ip` int(10) unsigned default NULL,
  `dateformat` varchar(10) NOT NULL default 'Y-m-d H:i:',
  PRIMARY KEY  (`user`),
  UNIQUE KEY `name` (`name`,`password`),
  KEY `last_active` (`last_active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


