<?php
$page_name = 'Current Poll';
require ('config.php');
if (!empty ($_POST['submit']))
{
	$require_login = true;
	$level_restriction = NEW_USER;
}
require ('top.inc.php');

$current_poll = mysql_result (mysql_query ('SELECT `pollid` FROM `poll` ORDER BY `pollid` DESC LIMIT 1'), 0);
$polls = isset ($_GET['pollid']) ?
mysql_query ('SELECT * FROM `poll` WHERE `pollname` != \'\' AND `pollid` <= '.intval ($_GET['pollid']).' ORDER BY `pollid` DESC'):
mysql_query ('SELECT * FROM `poll` WHERE `pollname` != \'\' ORDER BY `pollid` DESC');
$pollinfo = mysql_fetch_row ($polls);
if (!$pollinfo)
	stop ('Invalid poll ID.');
$prev_poll = mysql_query ('SELECT `pollid` FROM `poll` WHERE `pollname` != \'\' AND `pollid` < '.$pollinfo[0].' ORDER BY `pollid` DESC LIMIT 1');
$next_poll = mysql_query ('SELECT `pollid` FROM `poll` WHERE `pollname` != \'\' AND `pollid` > '.$pollinfo[0].' ORDER BY `pollid` ASC LIMIT 1');


if (isset ($_POST['submit']))
{
	if ($userinfo['lastpoll'] == $pollinfo[0])
		echo '<div class="alert">You have already voted in the current poll.</div>
';
	elseif (!isset ($userinfo['username']))
		echo '<div class="alert">You must be logged in to vote.</div>
';
	elseif (!$pollinfo[$_POST['voteid'] + 1])
		echo '<div class="alert">Invalid poll option.</div>
';
	else
	{
		$voteid = intval ($_POST['voteid']);
		mysql_query ('UPDATE `poll` SET `votes'.$voteid.'` = (`votes'.$voteid.'` + 1) WHERE `pollid` = '.$pollinfo[0]);
		mysql_query ('UPDATE `users` SET `lastpoll` = '.$pollinfo[0].', `lastvote` = '.$voteid.' WHERE `lastacip` = \''.$_SERVER['REMOTE_ADDR'].'\'');
		echo '<div class="alert">Vote submitted.</div>
';
		$pollinfo = mysql_fetch_row (mysql_query ('SELECT * FROM `poll` ORDER BY `pollid` DESC'));
	}
}

$sum = array_sum (array_slice ($pollinfo, 10, 8));
echo '<div class="c3">'.$pollinfo[1].': '.$sum.' votes, started '.date2 ($pollinfo[18]).'</div>
<table>
<tr><th style="width:20%">Option</th><th colspan="2">Votes</th></tr>
';
for ($optnum = 2; $optnum < 10; $optnum++)
{
	$widthpc = $sum ? round ($pollinfo[($optnum + 8)] / $sum * 90, 1) : 0;
	if ($pollinfo[$optnum] != '')
	{
		echo '<tr class='.colour().'><td title="'.($optnum-1).'">'.$pollinfo[$optnum].'</td><td style="width:2em">'.$pollinfo[($optnum + 8)].'</td>
<td><div class="alert" style="width:',$widthpc,'%;white-space:normal;overflow:visible">';
		if ($userinfo['level'] >= MOD && $current_poll == $pollinfo[0])
		{
			$voteusers = mysql_query ('SELECT `userid` FROM `users` WHERE `lastpoll` = '.$pollinfo[0].' AND `lastvote` = '.($optnum - 1));
			for ($un = 0; $usr = mysql_fetch_row ($voteusers); ++$un)
				echo ($un ? ', ' : 'Users: ').userlink($usr[0]);
		}
		else
			echo '&nbsp;';
		echo '</div></td></tr>
';
	}
}
echo '</table>
<div class="c3">';
if (mysql_num_rows ($prev_poll))
	echo '<a href="?pollid='.mysql_result ($prev_poll, 0).'" rel="previous">&larr; Previous</a>';
if (mysql_num_rows ($next_poll))
	echo ' | <a href="?pollid='.mysql_result ($next_poll, 0).'" rel="next">Next &rarr;</a> | <a href="poll.php">Current Poll</a>';

echo '</div>';

echo '<h2>Full List</h2>
<ol class="plain-list">';
$polls = mysql_query ('SELECT * FROM `poll` WHERE `pollname` != \'\' ORDER BY `pollid` DESC');
	while ($pollinfo = mysql_fetch_row ($polls))
		echo '<li class='.colour().'><a href="?pollid='.$pollinfo[0].'">'.date2 ($pollinfo[18]).': '.$pollinfo[1].'</a></li>'."\n";
echo '</ol>';

require ('foot.php');
?>