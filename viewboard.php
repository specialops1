<?php
require 'config.php';

$boardinfo = mysql_fetch_row(mysql_query('SELECT `name`, `view_level`, `topic_level` FROM `boards`
		WHERE `board` = '.intval($_GET['b'])));

if ( $boardinfo )
	list($page_name, $level_restriction, $topic_level) = $boardinfo;

require 'top.inc.php';

if ( empty($boardinfo) )
	printf('Invalid Board ID.') && footer();

$page = isset ($_GET['p']) ? intval($_GET['p']) : 1;
$pages = ($page - 1) * $userinfo['topics_page'];
$topic_count = $pages;

list($number_of_items) = mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `topics` WHERE `board` = '.intval($_GET['b'])));

if ( $number_of_items > $userinfo['topics_page'] ) {
	$count = 0;
	$pagelist = '<div class="c3">Page '.$page." | \n";
	while ( $count < $number_of_items / $userinfo['topics_page'] ) {
		$stuff = '  ';
		$rel = '';
		switch (++$count) {
			case $page:
				$stuff = '[]';
				break;
			case $page - 1:
				$rel = 'previous';
				break;
			case $page + 1:
				$rel = 'next';
				break;
		}
		$pagelist .= '<a href="?p='.$count.URL_APPEND.'"'.( $rel ? ' rel="'.$rel.'"' : '' ).'>'.$stuff{0}.$count.$stuff{1}."</a>\n";
	}
	$pagelist .= "</div>\n";
}
else
	$pagelist = null;

echo $pagelist.'
<table>
<thead>
<tr class="c3">
	<th>Topic Name</th>
	<th>Created By</th>
	<th>Posts</th>
	<th>Last Post</th>
</tr>
</thead>
<tbody>
';

$topic_list = mysql_query('SELECT `topics`.`topic`, `title`, `topics`.`visible`, `topics`.`user`,
			UNIX_TIMESTAMP(`time`) AS `time`, COUNT(`message`) AS `msgs`
		FROM `topics` LEFT JOIN `messages` USING(`topic`)
		WHERE `board` = '.intval($_GET['b']).
		( $userinfo['level'] >= MOD ? '' : ' AND `topics`.`visible` >= 0 AND `messages`.`visible` >= 0' ).'
		GROUP BY `topics`.`topic`
		ORDER BY `time` DESC
		LIMIT '.$pages.', '.$userinfo['topics_page']);

$n = 0;
$a = 'asdfg';

while ( $topic = mysql_fetch_assoc($topic_list) ) {
	$newposts = ($topic['time'] > time() - 3600) ? true : false;

	echo '<tr class=',colour(),'>
	<td><a href="viewtopic?t=',$topic['topic'],'"',( $n < 5 ? ' accesskey="'.$a{$n++}.'"' : '' ),'>',
			$topic['title'],'</a></td>
	<td>',userlink($topic['user']),'</td>
	<td>',$topic['msgs'],'</td>
	<td',( $newposts ? ' class="newposts">' : '>' ),date2($topic['time']),"</td>\n",
	"</tr>\n";
}

echo '</tbody>
</table>
'.$pagelist;

footer();
?>