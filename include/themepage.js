// this is based on the thing in phpMyAdmin that does the same thing, except mine does it worse

function showstuff()
{
	document.getElementById("premadebox").style.display = "none";
	document.getElementById("importbox").style.display = "none";
	document.getElementById("advancedbox").style.display = "none";

	if (document.getElementById("theme_pre").checked) {
		document.getElementById("premadebox").style.display = "block";
	} else if (document.getElementById("theme_adv").checked) {
		document.getElementById("advancedbox").style.display = "block";
	} else if (document.getElementById("theme_usr").checked) {
		document.getElementById("importbox").style.display = "block";
	}

	if (document.getElementById("premade_list").options.length == 0) {
		document.getElementById("theme_pre").disabled = true;
	}
	if (document.getElementById("import_list").options.length == 0) {
		document.getElementById("theme_usr").disabled = true;
	}
}

window.onload = showstuff;