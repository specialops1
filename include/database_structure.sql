-- phpMyAdmin SQL Dump
-- version 2.6.3-pl1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 02, 2005 at 07:00 PM
-- Server version: 5.0.10
-- PHP Version: 5.1.0b3
--
-- Database: `so2-dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `board-groups`
--

CREATE TABLE `board-groups` (
  `gid` tinyint(3) unsigned NOT NULL auto_increment,
  `g_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`gid`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `boardid` tinyint(4) NOT NULL auto_increment,
  `b_group` tinyint(3) unsigned NOT NULL default '0',
  `b_name` varchar(255) NOT NULL default '',
  `b_desc` varchar(255) NOT NULL default '',
  `b_view_level` tinyint(3) unsigned NOT NULL default '0',
  `b_topic_level` tinyint(3) unsigned NOT NULL default '0',
  `b_view_points` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`boardid`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `invites`
--

CREATE TABLE `invites` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `userid` smallint(5) unsigned NOT NULL,
  `code` char(36) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `userid` (`userid`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `mkid` tinyint(3) unsigned NOT NULL auto_increment,
  `mk_msgid` mediumint(8) unsigned NOT NULL default '0',
  `mk_time` timestamp NOT NULL default '0000-00-00 00:00:00',
  `mk_message` char(255) NOT NULL default '',
  `mk_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY  (`mkid`),
  KEY `mk_msgid` (`mk_msgid`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `message-data`
--

CREATE TABLE `message-data` (
  `m_id` mediumint(8) unsigned NOT NULL default '0',
  `m_text` text NOT NULL,
  PRIMARY KEY  (`m_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `messageid` mediumint(8) unsigned NOT NULL auto_increment,
  `m_topic` mediumint(8) unsigned NOT NULL default '0',
  `m_poster` smallint(5) unsigned NOT NULL default '0',
  `m_time` int(10) unsigned NOT NULL default '0',
  `m_state` tinyint(3) NOT NULL default '0',
  `m_replyto` mediumint(8) unsigned default NULL,
  `m_ip` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`messageid`),
  KEY `m_replyto` (`m_replyto`),
  KEY `m_poster` (`m_poster`),
  KEY `m_topic` (`m_topic`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE `styles` (
  `stid` tinyint(3) unsigned NOT NULL auto_increment,
  `st_name` char(20) NOT NULL default '',
  `st_file` char(20) NOT NULL default '',
  PRIMARY KEY  (`stid`)
) TYPE=MyISAM ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `topicid` mediumint(8) unsigned NOT NULL auto_increment,
  `t_name` varchar(255) NOT NULL default '',
  `t_poster` smallint(5) unsigned NOT NULL default '0',
  `t_board` tinyint(4) NOT NULL default '0',
  `t_visible` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`topicid`),
  KEY `t_poster` (`t_poster`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` smallint(5) unsigned NOT NULL auto_increment,
  `u_level` tinyint(3) NOT NULL default '10',
  `u_name` varchar(25) NOT NULL default '',
  `u_passwd` binary(255) NOT NULL,
  `u_points` int(10) unsigned NOT NULL default '0',
  `u_referrer` smallint(5) unsigned default NULL,
  `u_blist_view` tinyint(1) unsigned NOT NULL default '0',
  `u_tlist_view` tinyint(1) unsigned NOT NULL default '0',
  `u_mlist_view` tinyint(1) unsigned NOT NULL default '0',
  `u_topics_page` tinyint(3) unsigned NOT NULL default '35',
  `u_msgs_page` tinyint(3) unsigned NOT NULL default '35',
  `u_language` varchar(32) NOT NULL default 'en_GB',
  `u_timezone` tinyint(2) NOT NULL default '0',
  `u_theme` tinyint(3) unsigned NOT NULL default '4',
  `u_lastactive` int(10) unsigned NOT NULL default '0',
  `u_firstactive` int(10) unsigned NOT NULL default '0',
  `u_sig` varchar(255) NOT NULL default '',
  `u_quote` varchar(255) NOT NULL default '',
  `useragent` varchar(100) NOT NULL,
  `u_public_email` varchar(60) NOT NULL default '',
  `u_private_email` varchar(60) NOT NULL default '',
  `u_reg_email` varchar(60) NOT NULL default '',
  `u_lastip` int(10) unsigned default '0',
  `u_lastloginip` int(10) unsigned default '0',
  `u_regip` int(10) unsigned default '0',
  `u_date` varchar(30) NOT NULL default 'Y-m-d H:i:s',
  PRIMARY KEY  (`userid`),
  KEY `u_referrer` (`u_referrer`)
) TYPE=MyISAM;
