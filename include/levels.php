<?php
$cfg['leveldesc'] = array(
	KOS => 'KOSed: User is no longer allowed on the boards.',
	BANNED_USER => 'Banned: User has had their access privileges removed.',
	SUSPENDED_USER => 'Suspended: User has had their access privileges removed pending further action.',
	PENDING_EMAIL => 'Pending Activation: User has not yet activated their account.',
	CLOSED => 'Closed: Account has been disabled by the user.',
	PENDING_CLOSE => 'Pending Closure: Account is scheduled to be closed by the user.',
	INACTIVE_USER => 'Inactive: Account is pending activation by an administrator.',
	NEW_USER => 'New User',
	REG_USER => 'Regular User',
	VET_USER => 'Veteran User',
	ELI_USER => 'Elite User',
	VIP_USER => 'VIP: User has access to extra features.',
	MOD => 'Moderator: Can delete messages, suspend users.',
	ADMIN => 'Administrator: Can ban users and edit boards.',
	ADMIN2 => 'System Administrator: Has full control over the boards.'
);
?>
