<?php
class Message
{
	var $temp_text;
	var $xml;
	var $xml_string;

	var $error_line;
	var $error_code;
	var $error_message;
	var $error_description;
	var $output;

	var $M_HTML_ALL = 0;
	var $M_HTML_NONE = 1;
	var $M_HTML_FILTERED = 2;
	var $M_NO_NEWLINES = 4;
	var $M_URL_LENGTH = 70;

	var $allowed_html = array(
		'p', 'blockquote', 'q', 'ins', 'del', 'em', 'strong',
		'code', 'pre', 'var',
		'ol', 'ul', 'li', 'dl', 'dt', 'dd'
	);

	function Message($input, $formatting = null)
	{
		if ( is_null($formatting) )
			$formatting = $this->M_HTML_FILTERED;

		$this->temp_text = str_replace("\r\n", "\n", $input);
		$this->temp_text = htmlspecialchars($this->temp_text);

		if ( $formatting & $this->M_HTML_NONE ) { //no html
			null;
		} elseif ( $formatting & $this->M_HTML_FILTERED ) { //some html
			foreach ( $this->allowed_html as $tag )
				$preg1[] = '#&lt;('.$tag.')&gt;(.+)&lt;/('.$tag.')&gt;#Usie';
			$this->temp_text = preg_replace($preg1, "'<'.strtolower('$1').'>$2</'.strtolower('$3').'>'", $this->temp_text);
		} else { //all html
			$this->temp_text = html_entity_decode($this->temp_text);
			$this->temp_text = preg_replace('#(^|\s)(<|>)_(<|>)($|\s)#e', 'htmlspecialchars(\'$0\')', $this->temp_text);
		}

		if ( !($formatting & $this->M_HTML_NONE) ) { //anything except plain
			$this->temp_text = preg_replace('#(?<!")(http|https|ftp|irc)://(([0-9a-zA-Z\-_.+?/%=;:]|&amp;)+)#ie',
					"message::makeurl('$1://$2', ".$formatting.")",
					$this->temp_text);
		}

		if ( !($formatting & $this->M_NO_NEWLINES) ) {
			$this->temp_text = nl2br($this->temp_text);
			//Trim <br/>s after block elements
			$this->temp_text = preg_replace('#<(/?table|/?tr|/t[dh]|/?[duo]l|/d[dt]|/li|/p|/h[1-6])>(\s*<br />)+#si', '<$1>', $this->temp_text);
		}

		if ( $this->xml_validate() )
			$this->output = $this->temp_text;
		else
			$this->output = false;
	}

	function makeurl($uri, $formatting)
	{
		if ( $formatting & $this->M_HTML_FILTERED )
			$uri = html_entity_decode($uri);
		return '<a href="'.htmlentities($uri).'" rel="nofollow">'.
				( strlen($uri) > $this->M_URL_LENGTH ? htmlentities(substr($uri, 0, $this->M_URL_LENGTH)).'<strong>[...]</strong>' : $uri).
				'</a>';
	}

	function xml_validate()
	{
		// Check for broken XML, doesn't really do much else
		$this->xml = xml_parser_create('UTF-8');
		$this->xml_string = '<xhtml>'.$this->temp_text.'</xhtml>';

		if ( !xml_parse($this->xml, $this->xml_string, true) )
			return false;
		else
			return true;
	}
}
?>
