<?php
class Message {
	
	public $format_level = 2;
	public $line_breaks = 1;
	public $syntax_highlight = 0;
	
	private $temp_text = '';
	private $parser_object;
	private $parser_string;
	public $parser_error;
	public $parser_line;
	
	private $allowed_html = array ('i', 'b', 'p', 'ins', 'del', 'em', 'strong', 'tt', 'pre', 'ol', 'ul', 'li', 'dl', 'dt', 'dd', 'blockquote', 'q');
	
	function __construct ($userlevel, $input, $formatting, $line_breaks, $syntax)
	{
		// Sanitize input and add special vars
		$this->temp_text = str_replace (
			array ('&uagent&', '&ipaddr&'),
			array ($_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']),
			preg_replace ('/[\x01-\x09\x0b-\x1f\xa0\xad]/', ' ', str_replace ("\r\n", "\n", $input))
		);
		
		$this->line_breaks = intval ($line_breaks);
		switch ($formatting)
		{
			case 'none':
				$this->format_level = 0;
				break;
			case 'gfaqs':
				$this->format_level = 1;
				break;
			case 'full':
				// Stop unauthorized users from faking the all HTML setting
				$this->format_level = ($userlevel >= VIP_USER ? 3 : 2);
				break;
			default:
			case 'normal':
				$this->format_level = 2;
				break;
		}
		
		// Had to change the htmlentities to htmlspecialchars, might cause problems
		switch ($this->format_level)
		{
			case 0:
			// No HTML at all
				$this->temp_text = htmlspecialchars ($this->temp_text);
				break;
			case 1:
			// GameFAQs HTML: bold and italic only
				$this->temp_text = preg_replace ('#&lt;(b|i)&gt;(.*)&lt;/\1&gt;#Usie', "'<'.strtolower('$1').'>$2</'.strtolower('$1').'>'", htmlspecialchars ($this->temp_text));
				break;
			case 3:
			// All HTML: This regex fixes URLs and encodes <_>'s
				$this->temp_text = preg_replace ('# (<|>)_(<|>)|:(<|>) #ie', 'htmlspecialchars(\'$0\')', $this->temp_text);
				$this->temp_text = preg_replace ('#(http|ftp)://([0-9a-zA-Z\-_.+?/%=&;:]*)#ie', 'htmlspecialchars(\'$0\')', $this->temp_text);
				break;
			default:
			case 2:
			// Normal HTML
				foreach ($this->allowed_html as $tag)
					$preg1[] = '#&lt;('.$tag.')&gt;(.*)&lt;/\1&gt;#Usie';
				$this->temp_text = preg_replace ($preg1, "'<'.strtolower('$1').'>$2</'.strtolower('$1').'>'", htmlspecialchars ($this->temp_text));
				break;
		}
		
		// The manual line breaks option thing
		switch ($this->line_breaks)
		{
			case 1:
				$this->temp_text = nl2br ($this->temp_text);
				break;
			case 2:
				$this->temp_text = preg_replace ('#(&lt;|<)br ?\/?(&gt;|>)#i', '<br />', $this->temp_text);
				break;
		}
		
		// Fix PHP highlighter stuff for people with HTML disabled
		if (isset ($_POST['php_code']))
		{
			if ($this->format_level < 3)
				$this->temp_text = preg_replace ('#&lt;\?php(.*)\?&gt;#sie', "'<?php'.html_entity_decode('$1').'?>'", $this->temp_text);
			
			// Set the colours first
			ini_set ('highlight.comment', 'phpcomment');
			ini_set ('highlight.default', 'phpdefault');
			ini_set ('highlight.keyword', 'phpkeyword');
			ini_set ('highlight.string', 'phpstring');
			ini_set ('highlight.html', 'phphtml');
			// Then highlight stuff
			$this->temp_text = preg_replace ('#<\?php.*\?>#Usie', "'<blockquote class=\"php\">'.\$this->formatPHP('$0').'</blockquote>'", $this->temp_text);
		}
	}
	
	function formatPHP ($string)
	{
		// Fix HTML output of phpHighlight()
		$string = highlight_string ($string, true);
		$str1 = array ('&nbsp;', '<br />', ' <br />', '&nbsp;<br />', '<font color="', '</font>', '<span style="color: ', '  ');
		$str2 = array (' ', "<br />\n", '<br />', '<br />', '<span class="', '</span>', '<span class="', ' ');
		$string = str_replace ($str1, $str2, $string);
		
		// Get rid of newlines where they shouldn't be
		if ($this->line_breaks === 1)
			$string = str_replace ("<br />\n", '<br />', $string);
		
		return $string;
	}
	
	function getOutput()
	{
		// Just returns it.
		return $this->temp_text;
	}
	
	function isValid()
	{
		// XML parser to check for broken HTML
		$this->parser_object = xml_parser_create ('UTF-8'); // *wonders why I didn't think of using utf8 before*
		$this->parser_string = utf8_encode ('<div>'.$this->temp_text.'</div>');
		
		if (!xml_parse ($this->parser_object, $this->parser_string, true))
		{
			$this->parser_error = xml_get_error_code ($this->parser_object);
			$this->parser_line = xml_get_current_line_number ($this->parser_object);
			return false;
		}
		return true;
	}
		
	function getValidationError()
	{
		switch ($this->parser_error)
		{
			case 26:
				return '<dt>Unknown named entity (Line '.$this->parser_line.')</dt>
<dd>The XML parser does not recognise named HTML entities. Use numeric ones instead.</dd>';
			case 41:
				return '<dt>Attribute error (Line '.$this->parser_line.')</dt>
<dd>This is usually because you used unquoted attributes in your HTML tags.</dd>';
			case 76:
				return '<dt>Unclosed or extra ending tag (Line '.$this->parser_line.')</dt>
<dd>This usually means you closed your HTML tags in the wrong order or forgot to close them.</dd>';
			default:
				return '<dt>Unknown error #'.$this->parser_error.' (Line '.$this->parser_line.')</dt>
<dd>'.xml_error_string ($this->parser_error).'.</dd>';
		}
	}
}
?>