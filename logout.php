<?php
unset ($_COOKIE);
setcookie ('uname', '-', 1);
setcookie ('pword', '-', 1);
setcookie ('username', '-', 1);
setcookie ('password', '-', 1);
$page_name = 'Logout';

require ('config.php');
require ('top.inc.php');

echo '<div class="alert">You are now logged out and your cookie has been deleted.</div>
<h2>Links</h2>
<ul>
<li><a href="login.php">Log In</a></li>
<li><a href="./">Board List</a></li>
<li><a href="http://specialops.ath.cx/">Home Page</a></li>
<li><a href="http://boards.gamefaqs.com/">GameFAQs Message Boards</a></li>
</ul>';

require ('foot.php');
?>