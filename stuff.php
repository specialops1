<?php
$page_name = 'Miscellaneous stuff';
require 'config.php';
define('CSS_INC', 'stuff');
require 'top.inc.php';

$stats = array(
	mysql_fetch_row(mysql_query('SELECT COUNT(*), COUNT(DISTINCT `topic`), COUNT(DISTINCT `user`) FROM `messages`')),
	mysql_fetch_row(mysql_query('SELECT COUNT(*), COUNT(DISTINCT `topic`), COUNT(DISTINCT `user`) FROM `messages`
		WHERE `time` >= '.gmdate('YmdHis', time()-86400*30))),
	mysql_fetch_row(mysql_query('SELECT COUNT(*), COUNT(DISTINCT `topic`), COUNT(DISTINCT `user`) FROM `messages`
		WHERE `time` >= '.gmdate('YmdHis', time()-86400*7))),
	mysql_fetch_row(mysql_query('SELECT COUNT(*), COUNT(DISTINCT `topic`), COUNT(DISTINCT `user`) FROM `messages`
		WHERE `time` >= '.gmdate('YmdHis', time()-86400)))
);
$user_stats = mysql_fetch_row(mysql_query('SELECT SUM(`cookies`), SUM(`points`), COUNT(*) FROM `users`'));

echo '<h2>Contents</h2>
<ol class="c1">
<li><a href="#stats">Message Board Statistics</a></li>
<li><a href="#other">Other Stats</a></li>
<li><a href="#TOS">TOS</a></li>
<li><a href="#stuff">FAQ</a></li>
<li><a href="#info">More Info</a></li>
<li><a href="#credits">Credits</a></li>
<li><a href="docs/">Documentation &rarr;</a></li>
</ol>

<h2 id="stats">Message Board Statistics</h2>
<table>
<thead>
<tr>
	<td/>
	<th scope="col">Total</th>
	<th scope="col">Last 30 days</th>
	<th scope="col">Last 7 days</th>
	<th scope="col">Last 24 hours</th>
</tr>
</thead>
<tbody>
<tr class=',colour(),'>
<th scope="row">Topics</td>
<td>',$stats[0][1],'</td>
<td>',$stats[1][1],'</td>
<td>',$stats[2][1],'</td>
<td>',$stats[3][1],'</td>
</tr>
<tr class=',colour(),'>
<th scope="row">Messages</td>
<td>',$stats[0][0],'</td>
<td>',$stats[1][0],'</td>
<td>',$stats[2][0],'</td>
<td>',$stats[3][0],'</td>
</tr>
<tr class=',colour(),'>
<th scope="row">Users</td>
<td>',$user_stats[2],'</td>
<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `register_date` >= '.gmdate('YmdHis', time()-86400*30)), 0),'</td>
<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `register_date` >= '.gmdate('YmdHis', time()-86400*7)), 0),'</td>
<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `register_date` >= '.gmdate('YmdHis', time()-86400)), 0),'</td>
</tr>
<tr class=',colour(),'>
<th scope="row">Active Users</td>
<td>',$stats[0][2],'</td>
<td>',$stats[1][2],'</td>
<td>',$stats[2][2],'</td>
<td>',$stats[3][2],'</td>
</tr>
<tr class=',colour(),'>
<th scope="row">Lurkers</td>
<td>',($user_stats[2] - $stats[0][2]),'</td>
<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `last_active` >= '.gmdate('YmdHis', time()-86400*30)), 0),'</td>
<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `last_active` >= '.gmdate('YmdHis', time()-86400*7)), 0),'</td>
<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `last_active` >= '.gmdate('YmdHis', time()-86400)), 0),'</td>
</tr>
<tr class=',colour(),'>
<th scope="row">Posts per user</td>
<td>',($stats[0][2] > 0 ? round($stats[0][0] / $stats[0][2], 2) : 0),'</td>
<td>',($stats[1][2] > 0 ? round($stats[1][0] / $stats[1][2], 2) : 0),'</td>
<td>',($stats[2][2] > 0 ? round($stats[2][0] / $stats[2][2], 2) : 0),'</td>
<td>',($stats[3][2] > 0 ? round($stats[3][0] / $stats[3][2], 2) : 0),'</td>
</tr>
<tr class=',colour(),'>
<th scope="row">Posts per topic</td>
<td>',($stats[0][1] > 0 ? round($stats[0][0] / $stats[0][1], 2) : 0),'</td>
<td>',($stats[1][1] > 0 ? round($stats[1][0] / $stats[1][1], 2) : 0),'</td>
<td>',($stats[2][1] > 0 ? round($stats[2][0] / $stats[2][1], 2) : 0),'</td>
<td>',($stats[3][1] > 0 ? round($stats[3][0] / $stats[3][1], 2) : 0),'</td>
</tr>
</tbody>
</table>

<h2 id="other">Other Stats</h2>
<div class="c1">
<h3>General</h3>
<ul class="plain-list">
<li>Total ',$strings['cookies'],': ',$user_stats[0],'</li>
<li>Total ',$strings['points'],': ',$user_stats[1],'</li>
</ul>

<h3>Browser Usage</h3>
<ul class="plain-list">
<li><a href="http://mozilla.org/">Gecko</a>: ',
($browser1 = mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `useragent` LIKE \'%Gecko%\''), 0)),'</li>
<li><a href="http://kde.org/">KHTML</a>: ',
($browser2 = mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `useragent` LIKE \'%KHTML%\''), 0)),'</li>
<li>Other: ',($user_stats[2] - $browser1 - $browser2),'</li>
</ul>

<h3>Server info</h3>
<ul class="plain-list">
<li>PHP Version: ',PHP_VERSION,'</li>
<li>MySQL Version: ',mysql_get_server_info(),'</li>
</ul>
</div>';
?>

<h2 id="TOS">Terms of Service</h2>
<p>Don't be a fucking idiot.</p>

<h2 id="stuff">Stuff</h2>
<dl>
<dt>What's <?php echo $strings['cookies'] ?>?</dt>
<dd><?php echo $strings['cookies'] ?> is awarded by moderators for intelligent, funny, or otherwise good posts.
When you reach a certain amount you go up a level. Higher levels have more privileges and can access more boards.</dd>
<dt>What's <?php echo $strings['points'] ?>?</dt>
<dd><?php echo $strings['points'] ?> is a measure of user activity. It ranges from -20 to 50. You get one for each post you make, and when you reach the
upper limit you get an extra <?php echo $strings['cookies'] ?> and your <?php echo $strings['points'] ?> count is reset to 0. You lose 1 when the purge is
run if you log in on that day.</dd>
<dt>What HTML works in posts?</dt>
<dd>Just check the post page. There are several HTML options there and all allowed tags are shown underneath.</dd>
<dt>Why can I see the mod board on the board list when I'm not level [whatever]?</dt>
<dd>All boards are visible, you just can't go to the ones you're not allowed on. There's no point hiding them since they barely fill one screen anyway.</dd>
<dt>It looks like garbage.</dt>
<dd>Why aren't you doing anything to fix it instead of bitching about it?</dd>
<dt>How do I make a spinoff board?</dt>
<dd>If you have to ask that question, you are unfit to run anything above proboards.</dd>
<dt>I found a fatal exploit, where do I report it?</dt>
<dd>Either email me, IM me or IM someone on my closed list about it.</dd>
<dt>What the fuck why isn't strikethrough working</dt>
<dd>Short answer: You don't know how to use it. Long answer: http://goatse.ca/</dd>
<dt>Where do you want to go today?</dt>
<dd>Hell.</dd>
<!--[if IE]>
<dt>Teh boards are broked in mi internat EXPLORAR fizx thm!!1</dt>
<dd>No. Fuck you, commie bastard.</dd>
<!-[end if]-->
</dl>

<h2 id="info">Misc Info</h2>
<div class="c1">
<h3>Purge</h3>
<p>The purge is run nightly at 00:00 UTC. All user levels are updated at this time.
Unused accounts with no posts get purged on a nightly basis. The current schedule is as follows:</p>
<ul>
<li>Unconfirmed accounts (not email activated or not posted on unconfirmed board): 48 hours after last login</li>
<li>Banned accounts (level -2 and below): 5 months after last login</li>
<li>Suspended accounts: 3 months after last login</li>
<li>Other accounts: 1 year after last login</li>
</ul>

<h3>User Levels</h3>
<div style="max-width:40%; float:left">
<p>The current userlevels in use are shown on the right. As you earn a higher userlevel you gain more privileges.
Users of level <?php echo REG_USER ?> and above can use italic topic titles.
Users at level <?php echo VIP_USER ?> and above can use all HTML in posts.
A higher userlevel also gives you access to more boards; see the table below for minimum level requirements:</p>

<table>
<caption>Board Levels List</caption>
<tr><th>Board</th><th>Userlevel</th></tr>
<?php
$tmp = mysql_unbuffered_query('SELECT `board`, `name`, `view_level` FROM `boards` ORDER BY `view_level` ASC');
while ( $b = mysql_fetch_row($tmp) )
	echo '<tr class=',colour(),'><td><a href="viewboard?b=',$b[0],'">',$b[1],'</a></td><td>',$b[2],"</td></tr>\n";
?>
</table>
</div>

<table style="width:55%; margin:1em">
<caption>User Levels List</caption>
<tr><th>Level</th><th>Name</th><th>Users</th></tr>
<?php
include 'include/levels.php';

foreach ( array_keys($cfg['levels']) as $level ) {
	echo '<tr class=',colour(),'><td>',$level,'</td><td>',$cfg['leveldesc'][$level],'</td>',
		'<td>',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `level` = '.$level), 0),"</td></tr>\n";
}
?>
</table>
</div>

<h2 id="credits">Credits</h2>
<ul class="c1 plain-list">
<li>NeoGenesis - for making this shitty code.</li>
<li>Aqua/XPinion - hosted SO1 while I didn't have my own server.</li>
<li>Jay - thanks for nothing, asshole</li>
</ul>
<?php footer() ?>
