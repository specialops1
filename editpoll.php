<?php
$require_login = true;
$page_name = 'Poll Maker';
require ('config.php');
$level_restriction = ADMIN;
require ('top.inc.php');

if (isset ($_POST['submit']))
{
	$query1 = '';
	$query2 = '';
	for ($i = 1; $i <= 8; $i++)
	{
		$query1 .= '`option'.$i.'`, ';
		$query2 .= '\''.mysql_real_escape_string ($_POST['option'.$i]).'\', ';
	}
	$pollname = mysql_real_escape_string ($_POST['pollname']);

	mysql_query ('INSERT INTO `poll` (`pollname`, '.$query1.' `polldate`) VALUES (\''.$pollname.'\', '.$query2.' UNIX_TIMESTAMP())');
}

echo '<div class="c3">Start New Poll</div>
<form action="'.urlpath(2).'" method="post">
<div class='.colour().'>Poll name: <input type="text" name="pollname" maxlength="50"/></div>';

for ($optnum = 1; $optnum <= 8; $optnum++)
	echo '<div class='.colour().'>Option '.$optnum.': <input type="text" name="option'.$optnum.'" maxlength="100"/></div>'."\n";

echo '<div class="c3"><input type="submit" name="submit" value="Create new poll"/></div>
</form>
';

$pollinfo = mysql_fetch_row (mysql_query ('SELECT * FROM `poll` ORDER BY `pollid` DESC LIMIT 1'));
if ($pollinfo)
{
	$sum = array_sum (array_slice ($pollinfo, 10, 8));
	echo '<div class="c3">Current poll results for: '.$pollinfo[1].': '.$sum.' votes</div>
<table>
<tr><th>Option</th><th style="width:66%">Votes</th></tr>
';
	for ($optnum2 = 0; $optnum2 < 8; $optnum2++)
	{
		$widthpc = ($sum && $pollinfo[($optnum2+10)]) ? round ($pollinfo[($optnum2+10)] / $sum * 100, 1) : 0;
		if ($pollinfo[($optnum2+2)])
			echo '<tr class='.colour().'><td>'.$pollinfo[($optnum2+2)].'</td><td><span class="alert" style="width:'.$widthpc.'%;display:block">'.$pollinfo[($optnum2+10)].'</span></td></tr>
';
	}
	echo '</table>';
}

require ('foot.php');
?>