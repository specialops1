<?php
require 'config.php';
$level_restriction = INACTIVE_USER;
$require_login = true;

if ( isset($_POST['submit'], $_POST['themetype']) ) {

	if ( !function_exists('file_put_contents') ) {
		function file_put_contents($filename, $data) {
			$a = fopen($filename, 'w');
			fwrite($a, $data);
			fclose($a);
		}
	}

	$theme = 0;

	if ( $_POST['themetype'] == 'premade' && isset($_POST['theme']) ) {
		$theme = isset($_POST['theme']) ? intval($_POST['theme']) : 1;
		if ( isset($_POST['importpre']) ) {
			$theme = 0;
			$_POST['csstext'] = file_get_contents('csslib/system/'.mysql_result(
				mysql_unbuffered_query('SELECT `file` FROM `styles` WHERE `style` = '.$theme), 0));
		}
	}
	elseif ( $_POST['themetype'] == 'import' && isset($_POST['import']) ) {
		$theme = 0;
		$_POST['csstext'] = file_get_contents('csslib/user/'.intval($_POST['import']).'.css');
	}

	mysql_query ('UPDATE `users` SET `theme` = '.$theme.' WHERE `user` = '.$userinfo['user']);
	file_put_contents('csslib/user/'.$userinfo['user'].'.css', $_POST['csstext']);

	$userinfo = mysql_fetch_assoc(mysql_unbuffered_query('SELECT * FROM `users` WHERE `user` = '.$userinfo['user']));
}

$page_name = 'Theme Settings';
require 'top.inc.php';

if ( isset($_POST['submit']) || isset($_POST['import']) )
	echo '<p class="alert">Theme Updated.</p>';

echo '<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">

<fieldset style="float:left; width:25%">
<legend>Settings</legend>
<ul class="plain-list">
	<li><label><input type="radio" id="theme_pre" name="themetype" value="premade"',
		( $userinfo['theme'] > 0 ? ' checked="checked"' : '' ),' onclick="showstuff();"/>
		Premade (',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `theme` > 0'), 0),' user[s])</label></li>
	<li><label><input type="radio" id="theme_adv" name="themetype" value="advanced"',
		( $userinfo['theme'] == 0 ? ' checked="checked"' : '' ),' onclick="showstuff();"/>
		Advanced</label></li>
	<li><label><input type="radio" id="theme_usr" name="themetype" value="import" onclick="showstuff();"/>
		Import (',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `theme` = 0'), 0),' user[s])</label></li>
</ul>
<hr/>

Header: <select name="header">',"\n";
asort($cfg['headers']);
$i = 0;
foreach ( $cfg['headers'] as $desc => $image )
	echo '<option value="',++$i,'"',( $userinfo['header'] == $i ? ' selected="selected"' : '' ),'>',$desc,"</option>\n";
echo '</select>

<input type="submit" name="submit" accesskey="s" value="Save Settings (S)"/></fieldset>

<fieldset style="float:left; clear:left; width:25%">
<legend>Preview</legend>
<h2>Tables</h2>
<table style="width:100%">
<tr><th>tr th | <a href="#">Link</a></th></tr>',"\n";
for ( $row = 3; $row > 0; $row-- )
	echo '<tr class="c',$row,'"><td>tr.c',$row,' td | <a href="#">Link</a></td></tr>',"\n";
echo '</table>
<h3>Divs</h3>
<div class="alert">div.alert | <a href="#">Link</a></div>';
for ( $row = 3; $row > 0; $row-- )
	echo '<div class="c',$row,'">div.c',$row,' | <a href="#">Link</a></div>',"\n";

echo '<a href="csslib">CSS directory</a>
</fieldset>

<fieldset style="vertical-align:top" id="premadebox">
<legend>Premade Themes</legend>',"\n";

$themes = mysql_query('SELECT `style`, `title` FROM `styles` ORDER BY `title` ASC');
echo '<select name="theme" id="premade_list" size="',( ($i=mysql_num_rows($themes)) > 6 ? 6 : $i ),"\">\n";
while ( $style = mysql_fetch_row($themes) )
	echo '<option value="',$style[0],'"',( $userinfo['theme'] == $style[0] ? ' selected="selected"' : '' ),'>',$style[1],
		' (',mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE `theme` = '.$style[0]), 0)," users)</option>\n";

echo '</select>
<p>Choose from one of the default themes.</p>

<label><input type="checkbox" name="importpre"/> Import to advanced</label>
</fieldset>

<fieldset style="vertical-align:top" id="importbox">
<legend>Import User Theme</legend>',"\n";

$themes = mysql_query('SELECT `user`, `name`, `theme` FROM `users` WHERE `theme` = 0 ORDER BY `name` ASC');
echo '<select name="import" id="import_list" size="',( ($i=mysql_num_rows($themes)) > 6 ? 6 : $i ),"\">\n";
while ( $user = mysql_fetch_row($themes) )
	echo '<option value="',$user[0],'">',$user[1],"</option>\n";
echo '</select>

<p>Import another user\'s theme.</p>
</fieldset>

<fieldset style="vertical-align:top" id="advancedbox">
<legend>Advanced Custom Theme</legend>
<textarea name="csstext" rows="30" cols="80" style="height:25em; width:95%">';
if ( file_exists('csslib/user/'.$userinfo['user'].'.css') )
	echo htmlentities(file_get_contents('csslib/user/'.$userinfo['user'].'.css'));
echo '</textarea>
<p>Write your own CSS theme or modify an imported one.</p>
</fieldset>

</form>';

require 'foot.php';
?>
