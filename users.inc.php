<?php
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$pages = ($page -1) * 50;
$user_count = $pages;

$number_of_items = mysql_result(mysql_query('SELECT COUNT(*) FROM `users` WHERE '.$list_criteria), 0);

if ( !isset($_GET['order']) )
	$_GET['order'] = 'user';
if ( !isset($_GET['list']) )
	$_GET['list'] = 'all';

$pagelist = '';
if ( $number_of_items > 50 )
	$pagelist = pagelist(';order='.htmlentities($_GET['order']).';list='.htmlentities($_GET['list']));

echo $pagelist,'
<table>
<tr class="c3">';

foreach ( array(
		'ID #' => 'user',
		'Username' => 'name',
		'Level' => 'level',
		$strings['cookies'] => 'cookies',
		$strings['points'] => 'points',
		'Posts' => 'posts',
		'Last Active (Idle Time)' => 'last_active'
	) as $colname => $sqlname )
	echo '<th><a href="?order=',$sqlname,';list=',htmlentities($_GET['list']),URL_APPEND,'">',$colname,"</a></th>\n";

if ( $userinfo['level'] >= ADMIN )
	echo '<th><a href="?order=last_ip;list=',htmlentities($_GET['list']),URL_APPEND,'">Last IP</a></th>',"\n";
echo "</tr>\n";

switch ( $_GET['order'] ) {
	case 'name' :
	case 'user' :
	case 'last_ip' :
		$by = 'ASC';
		break;
	case 'level' :
	case 'cookies' :
	case 'posts' :
	case 'points' :
	case 'last_active' :
		$by = 'DESC';
		break;
	default :
		$_GET['order'] = 'user';
		$by = 'ASC';
}

$tmp = mysql_query('SELECT
	`users`.`user`,
	`users`.`level`,
	`users`.`cookies`,
	UNIX_TIMESTAMP(`users`.`last_active`),
	INET_NTOA(`users`.`last_ip`),
	COUNT(`messages`.`message`) AS `posts`,
	`users`.`points`

	FROM `users` LEFT JOIN `messages` USING(`user`)
	WHERE '.$list_criteria.'
	GROUP BY `users`.`user`
	ORDER BY `'.mysql_real_escape_string($_GET['order']).'` '.$by.', `users`.`user`
	LIMIT '.$pages.', 50');

while ( $u = mysql_fetch_row($tmp) )
{
	echo '<tr class=',colour(),'>
	<td>',$u[0],'</td>
	<td>',userlink($u[0]),'</td>
	<td>',$u[1],'</td>
	<td>',$u[2],'</td>
	<td>',$u[6],'</td>
	<td>',$u[5],'</td>
	<td>',date2($u[3]),' <small>(',idletime($u[3]),")</small></td>\n".
	( $userinfo['level'] >= ADMIN ? "\t<td>".$u[4]."</td>\n" : '').
"</tr>\n";
}

echo "</table>\n",$pagelist;
?>
