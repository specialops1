<?php
require ('config.php');
$topicinfo = mysql_fetch_row (mysql_query ('SELECT `topicname` FROM `topics` WHERE `topicid` = '.intval ($_GET['t'])));
$level_restriction = ADMIN;
$require_login = true;
$page_name = 'Edit Topic';
$title_name = $page_name.' ('.$topicinfo[0].')';
require ('top.inc.php');

if (isset ($_POST['submit']) && strstr ($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']))
{
	extract ($_POST);
	if (isset ($tdelete))
	{
		mysql_query ('DELETE FROM `topics` WHERE `topicid` = '.intval ($_GET['t']).' LIMIT 1');
		$kill = mysql_query ('SELECT `msgid` FROM `msgmeta` WHERE `topic` = '.intval ($_GET['t']));
		while ($message = mysql_fetch_row ($kill))
		{
			mysql_query ('DELETE FROM `msgmeta` WHERE `msgid` = '.$message[0]);
			mysql_query ('DELETE FROM `messages` WHERE `messageid` = '.$message[0]);
		}
		echo '<div class="alert">Topic deleted.</div>
';
	}
	else
	{
		mysql_query ('UPDATE `topics` SET
`topicname` = \''.mysql_real_escape_string ($_POST['tname']).'\',
`boardnum` = '.intval ($_POST['tboard']).',
`active` = \''.mysql_real_escape_string ($_POST['tactive']).'\'
		WHERE `topicid` = '.intval ($_GET['t']));
		mysql_query ('UPDATE `msgmeta` SET `mesboard` = \''.intval ($_POST['tboard']).'\' WHERE `topic` = '.intval ($_GET['t']));
		echo '<div class="alert">Topic updated.</div>
';
	}
}
else
{
	$myrow = mysql_fetch_assoc (mysql_query ('SELECT * FROM `topics` WHERE `topicid` = '.intval ($_GET['t'])));
	if (!$myrow)
		stop ('Invalid topic ID.');
	$boards = mysql_query ('SELECT `boardid`, `boardname` FROM `boards` WHERE `boardlevel` <= '.$userinfo['level'].' ORDER BY `theorder` ASC');
	
	echo '<form method="post" action="'.urlpath(2).'">
<dl>
<dt>Topic Name</dt>
<dd><input type="text" name="tname" style="width:100%; display: block" value="'.htmlentities ($myrow['topicname']).'"/></dd>
<dt>Location</dt>
<dd><select name="tboard">
';
	while ($levdef = mysql_fetch_row ($boards))
		echo '<option value="'.$levdef[0].'"'.($myrow['boardnum'] == $levdef[0] ? ' selected="selected"' : '').'>'.$levdef[1].'</option>'."\n";
	echo '</select></dd>
<dt>Topic status</dt>
<dd><ol>
<li><input type="radio" name="tactive" id="active" value="active" checked="checked"/> <label for="active">Active</label></li>
<li><input type="radio" name="tactive" id="closed" value="closed"/> <label for="closed">Closed</label></li>
<li><input type="radio" name="tactive" id="deleted" value="deleted"/> <label for="deleted">Deleted</label></li>
</ol></dd>
<dt><label for="delete">Destroy Topic</label></dt>
<dd><input type="checkbox" id="delete" name="tdelete"/> <label for="delete">- This permanently deletes the topic and messages from the database.</label></dd>
</dl>
<div class="c3"><input type="submit" name="submit" value="Update"/></div>
</form>
';
}

require ('foot.php');
?>