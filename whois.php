<?php
require 'config.php';
$require_login = true;
$page_name = 'User Information';
$title_name = 'Whois';
require 'top.inc.php';
include 'include/levels.php';

if ( isset($_POST['submit']) && ADMIN <= $userinfo['level']
		&& mysql_result(mysql_query('SELECT MD5(`name`) FROM `users` WHERE `user` = '.intval($_GET['u'])), 0) == $_POST['hash'] ) {
	if ( 'Ban User' == $_POST['submit'] ) {
		mysql_query('UPDATE `users` SET `level` = '.BANNED_USER.' WHERE `user` = '.intval($_GET['u']));
		echo '<div class="alert">User banned.</div>';
	}
	elseif ( 'Flood Control' == $_POST['submit'] ) {
		$topics = mysql_query('SELECT `topic` FROM `topics` WHERE `user` = '.intval($_GET['u']));

		while ($topic = mysql_fetch_row($topics)) {
			mysql_query('DELETE FROM `topics` WHERE `topic` = '.$topic[0].' LIMIT 1');
			$msgs = mysql_query('SELECT `message` FROM `messages` WHERE `topic` = '.$topic[0]);

			while ($msg = mysql_fetch_row($msgs)) {
				mysql_query('DELETE FROM `messages` WHERE `message` = '.$msg[0]);
				mysql_query('DELETE FROM `message-data` WHERE `message` = '.$msg[0]);
			}
		}

		$msgs = mysql_query('SELECT `message` FROM `messages` WHERE `user` = '.intval($_GET['u']));

		while ( $msg = mysql_fetch_row($msgs) ) {
			mysql_query('DELETE FROM `messages` WHERE `message` = '.$msg[0]);
			mysql_query('DELETE FROM `message-data` WHERE `message` = '.$msg[0]);
		}

		mysql_query('OPTIMIZE TABLE `message-data`, `messages`, `topics`');
		echo '<div class="alert">User\'s posts have been deleted.</div>';
	}
	elseif ( 'Confirm Account' == $_POST['submit'] ) {
		mysql_query('UPDATE `users` SET `level` = '.NEW_USER.' WHERE `user` = '.intval ($_GET['u']));
		echo '<div class="alert">User activated.</div>';
	}
}

if ( isset($_POST['suspend']) && MOD <= $userinfo['level'] ) {
	mysql_query('UPDATE `users` SET `level` = '.BANNED_USER.' WHERE `user` = '.intval ($_GET['u']));
	echo '<div class="alert">User suspended.</div>';
}

$whois = mysql_fetch_assoc(mysql_query('SELECT * FROM `users` WHERE `user` = '.intval($_GET['u'])));
if ( !$whois )
   stop('Invalid user ID.');

$posts = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages` WHERE `user` = '.$whois['user'].' AND `visible` >= 0'), 0);
$topics = mysql_result(mysql_query('SELECT COUNT(*) FROM `topics` WHERE `user` = '.$whois['user'].' AND `visible` >= 0'), 0);

switch ( $whois['theme'] ) {
	case 0:
		$usertheme = 'Custom
<form action="theme.php" method="post">
<input type="hidden" name="usertheme" value="'.intval($_GET['u']).'"/>
<input type="submit" name="import" value="Use"/>
<input type="submit" name="preview" value="Preview"/>
</form>';
		break;
	default:
		$usertheme = mysql_result(mysql_query('SELECT `title` FROM `styles` WHERE `style` = '.$whois['theme']), 0);
		break;
}

echo '<table>
<tr class='.colour().'><td>User ID</td><td>',$whois['user'],'</td></tr>
<tr class=',colour(),'><td>Username</td><td>',$whois['name'],'</td></tr>
<tr class=',colour(),'><td>User Level</td><td>',$cfg['leveldesc'][$whois['level']],'</td></tr>
<tr class=',colour(),'><td>',$strings['cookies'],'</td><td>',$whois['cookies'],'</td></tr>
<tr class=',colour(),'><td>',$strings['points'],'</td><td>',$whois['points'],'</td></tr>
<tr class=',colour(),'><td>Posts</td><td>',$posts,'</td></tr>
<tr class=',colour(),'><td>Topics</td><td>',$topics,'</td></tr>
<tr class=',colour(),'><td>Register Date</td><td>',$whois['register_date'],' (account age: '.floor((time() - strtotime($userinfo['register_date'])) / 86400).' days)</td></tr>
<tr class=',colour(),'><td>Last Activity Date</td><td>',$whois['last_active'],'</td></tr>
<tr class=',colour(),'><td>Idle time</td><td>',idletime(strtotime($whois['last_active'])),'</td></tr>
<tr class=',colour(),'><td>Theme</td><td>',$usertheme,'</td></tr>
<tr class=',colour(),'><td>Public Email Address</td><td><a href="mailto:',$whois['public_email'],'">',$whois['public_email'],'</a></td></tr>
<tr class=',colour(),'><td>Sig</td><td>',nl2br($whois['sig']),'</td></tr>
<tr class=',colour(),'><td>Quote</td><td>',$whois['quote'],'</td></tr>';

if ( $userinfo['level'] >= MOD )
	echo '<tr class=',colour(),'><td>Private Email Address</td><td><a href="mailto:',$whois['private_email'],'">',$whois['private_email'],'</a></td></tr>';

if ( $userinfo['level'] >= ADMIN ) {
	$regip = long2ip($whois['register_ip']);
	$lastip = long2ip($whois['last_ip']);
	echo '<tr class=',colour(),'><td>Signup Email Address</td><td><a href="mailto:',$whois['register_email'],'">',$whois['register_email'],'</a></td></tr>
<tr class=',colour(),'><td>Registration IP</td><td><a href="ipinfo.php?ip=',$regip,'">',$regip,'</a></td></tr>
<tr class=',colour(),'><td>Last IP</td><td><a href="ipinfo.php?ip=',$lastip,'">',$lastip,'</a></td></tr>';
echo '
<tr class='.colour().'><td>Browser</td><td>'.$whois['useragent'].'</td></tr>
</table>';
}

if ( $userinfo['level'] < MOD )
	stop();

echo '
<h2>Moderator Tools</h2>
<ul>
<li><a href="posthistory.php?user=',$whois['user'],URL_APPEND,'">Message History</a></li>
</ul>
<form method="post" action="?u=',$whois['user'],URL_APPEND,'">
<input type="hidden" name="hash" value="',md5($whois['name']),'"/>
<input type="submit" name="suspend" value="Suspend User" style="color:#fff; background:#f80; border-color:#f80; font-weight: bold"/>
</form>';

if ( $userinfo['level'] < ADMIN )
	footer();

echo '
<h2>Admin Tools</h2>
<ul>
<li><a href="edituser.php?user=',$whois['user'],URL_APPEND,'">Edit userinfo</a></li>
</ul>
<form method="post" action="?u=',$whois['user'],URL_APPEND,'">
<input type="hidden" name="hash" value="'.md5($whois['name']).'"/>
<input type="submit" name="submit" value="Ban User" style="color:#fff; background:#f00; border-color:#f00; font-weight: bold"/>
<input type="submit" name="submit" value="Flood Control" style="color:#000; background:#ff0; border-color:#ff0; font-weight: bold"/>',( $whois['level'] == INACTIVE_USER ? '
<input type="submit" name="submit" value="Confirm Account" style="color:#000; background:#0f0; border-color:#0f0; font-weight: bold"/>' : '' ),
'</form>';

footer();
?>