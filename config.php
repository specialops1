<?php
/*
 * Special Ops One message boards
 * Copyright © 2002-2005 Anthony Parsons
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * See file docs/LICENSE for full license terms.
 */

ob_start();

if ( version_compare(PHP_VERSION, '5.0', '<') ) {
	header('HTTP/1.1 500 Internal Server Error');
	die('PHP 5.0 or higher is required.');
}

error_reporting(E_ALL|E_STRICT);

// DB Connection
require 'include/sqlconn.php';

// Timer thing at bottom of page
define('START_TIME', microtime(1));

// Board text strings
$strings = array(
	'name' => 'MINESWEEPERBOARDOJ',
	'cookies' => 'Aura',
	'points' => 'Vim'
);

//  Image strings
$cfg['img'] = array(
	'<img src="imglib/add" alt="Added '.$strings['cookies'].'" class="imgadd"/>',
	'<img src="imglib/take" alt="Taken '.$strings['cookies'].'" class="imgtake"/>',
	'<img src="imglib/closed" alt="*Closed*" class="imgclose"/>',
	'<img src="imglib/deleted" alt="*Deleted*" class="imgdel"/>'
);

//custom header strings
$cfg['headers'] = array(
	'Text-Only' => 'Special Ops Boards'
);

$cfg['headers2'] = array_values($cfg['headers']);

//userlevel defines
$cfg['levels'] = array(
	-3 => 'KOS',
	-2 => 'BANNED_USER',
	-1 => 'SUSPENDED_USER',
	0 => 'PENDING_EMAIL',
	2 => 'CLOSED',
	3 => 'PENDING_CLOSE',
	4 => 'INACTIVE_USER',
	10 => 'NEW_USER',
	15 => 'REG_USER',
	20 => 'VET_USER',
	30 => 'ELI_USER',
	40 => 'VIP_USER',
	50 => 'MOD',
	55 => 'ADMIN',
	60 => 'ADMIN2'
);
foreach ( $cfg['levels'] as $number => $level )
	define($level, $number);

// user stuff
if ( isset($_COOKIE['userid'], $_COOKIE['password']) )
	$u = mysql_fetch_assoc(mysql_unbuffered_query('SELECT * FROM `users` WHERE `user` = '.intval($_COOKIE['userid']).' LIMIT 1'));

isset($_SERVER['HTTP_USER_AGENT']) ? null : $_SERVER['HTTP_USER_AGENT'] = '-';

if ( !empty($u) && $u['password'] === $_COOKIE['password'] ) {
	$userinfo = $u;
	$userinfo['level'] = intval($userinfo['level']);
	mysql_query('UPDATE `users` SET
		`last_active` = NOW(),
		`last_ip` = INET_ATON(\''.$_SERVER['REMOTE_ADDR'].'\'),
		`useragent` = \''.mysql_real_escape_string(htmlentities($_SERVER['HTTP_USER_AGENT'])).'\'
		WHERE `user` = '.$userinfo['user'].' LIMIT 1');

	$userinfo['timeoffset'] = $userinfo['timezone'] * 3600;	//bitfields are gay
}
else
{
	setcookie('userid', '-', 1);
	setcookie('password', '-', 1);
	$userinfo = array('header' => 0, 'timezone' => 0, 'topics_page' => 35, 'messages_page' => 35, 'level' => PENDING_EMAIL);
}

/* Global Functions */
// Bar shader
function colour()
{
	static $c = 0;
	return '"c'.(1+(++$c&1)).'"';
}

// Date formatting
function date2($timestamp = 0)
{
	global $userinfo;
	if ( empty($userinfo['dateformat']) )
		return gmdate('Y-m-d H:i:s', $timestamp + $userinfo['timezone']);
	return gmdate($userinfo['dateformat'], $timestamp + $userinfo['timezone']);
}

// Page exit
function stop ($msg = '')
{
	echo $msg;
	trigger_error('Use of stop() is deprecated - echo the message and call footer() directly', E_USER_WARNING);
	print_r(debug_backtrace());
	footer();
}

$urlstring = isset($_GET['b']) ? 'b='.intval($_GET['b']).( isset($_GET['t']) ? ';t='.intval($_GET['t']) : '' ) : '';
define('URL_STRING', '?'.$urlstring);
define('URL_APPEND', ';'.$urlstring);

// Returns idletime (not used much now)
function idletime($lastsec)
{
	return(time() - $lastsec > 86400 ? floor((time() - $lastsec) / 86400).'d ' : '').gmdate('G\hi\ms\s', time() - $lastsec);
}

// Makes userids into links
function userlink($id, $name = null)
{
	if ( empty($id) )
		return '';

	static $users;
	global $userinfo;

	if ( empty($users[$id]) )
		if ( empty($name) )
			list($users[$id]) = mysql_fetch_row(mysql_query('SELECT `name` FROM `users` WHERE `user` = '.intval($id)));
		else
			$users[$id] = $name;
	if ( empty($userinfo['user']) )
		return $users[$id];
	else
		return '<a href="whois?u='.$id.URL_APPEND.'">'.$users[$id].'</a>';
}

function footer()
{
?>
</div>

<p><a href="http://specialops.ath.cx/">Special Ops 1.25-dev</a> |
	<?php echo round(microtime(1) - START_TIME, 4) ?>s |
	© 2003-2005 <a href="http://specialops.ath.cx/contact">Ant P</a></p>
</body>
</html>
<?php
	exit;
}
?>
