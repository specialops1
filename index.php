<?php
$title_name = 'Board List';
$page_name = '';
require 'config.php';
require 'top.inc.php';

echo '<table style="clear:both">
<thead>
<tr>
	<th style="width:35%">Board</th>
	<th style="width:10%">Posts</th>
	<th style="width:10%">Topics</th>
	<th style="width:45%">Last Post</th>
</tr>
</thead>',"\n";

$sql = mysql_query('SELECT `group`, `name` FROM `board-groups` ORDER BY `group` ASC');
while ( list($cat_id, $cat_name) = mysql_fetch_row($sql) ) {
	echo "<tbody>\n",
		'<tr id="cat'.$cat_id.'"><th colspan="4"><a href="#cat',$cat_id,'">',$cat_name,"</a></th></tr>\n";

	$list = mysql_query('SELECT `board`, `name`, `caption`, `view_level` FROM `boards`
			WHERE `group` = '.$cat_id.' ORDER BY `board` ASC');

	while ( $board = mysql_fetch_assoc($list) ) {

		if ( $userinfo['level'] < $board['view_level'] ) {
			echo '<tr class="c2"><td>',$board['name'],"<br/>\n",
				"\t<small>",$board['caption'],"</small></td>\n",
				'<td colspan="3">-</td></tr>',"\n";
			continue;
		}

		$topic_ids = null;
		$tmp = mysql_query('SELECT `topic` FROM `topics` WHERE `board` = '.$board['board']);
		while ( $topic_id = mysql_fetch_row($tmp) )
			$topic_ids[] = $topic_id[0];

		list($messages) = count($topic_ids) ?
					mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `messages`
						WHERE `visible` >= 0 AND `topic` IN ('.implode(',', $topic_ids).')'))
					: array(0); /*</tabwhoring>*/

		echo '<tr class="c1">',"\n",
			'<td><a href="viewboard?b=',$board['board'],'">',$board['name'],"</a><br/>\n",
			"\t<small>",$board['caption'],"</small></td>\n",
			'<td>',$messages,"</td>\n",
			'<td>',count($topic_ids),"</td>\n";

		if ( $messages ) {
			$lastmsg = mysql_fetch_row(mysql_unbuffered_query(
				'SELECT UNIX_TIMESTAMP(`time`), `messages`.`user`, `topics`.`topic`, `title`
					FROM `messages` LEFT JOIN `topics` USING (`topic`)
					WHERE `board` = '.$board['board'].'
					AND `messages`.`visible` >= 0
					ORDER BY `message` DESC LIMIT 1'));
			echo '<td>',date2($lastmsg[0]),' <small>by ',userlink($lastmsg[1]),"<br/>\n",
				'in <a href="viewtopic?b=',$board['board'],';t=',$lastmsg[2],'">',strip_tags($lastmsg[3]),"</a></small></td>\n";
		} else
			echo '<td>none</td>';

		echo "</tr>\n";
	}
	echo "</tbody>\n";
}

mysql_free_result($sql);

// Start of info panel
$list = mysql_query('SELECT `user` FROM `users`
		WHERE `last_active` > (NOW() - 600)
		AND `level` > '.INACTIVE_USER.'
		ORDER BY `last_active` DESC LIMIT 20');

for ( $online_count = 0, $online_list = ''; list($tmp) = mysql_fetch_row($list); $online_count++ )
	$online_list .= '<li>'.userlink($tmp)."</li>\n";

mysql_free_result($list);

list($total_users) = mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `users`'));

echo '</table>

<div class="c3"><a href="stuff" accesskey="s">Stats and other stuff</a></div>
<p class="c2">';
if ( isset($userinfo['user']) )
	echo '<a href="userdir?list=online">',$online_count,' ',ngettext('user', 'users', $online_count),
		' online</a> out of <a href="userdir">',$total_users,' total</a>';
else
	echo $online_count,' ',ngettext('user', 'users', $online_count),' online out of ',$total_users,' total';
echo "</p>\n",
( $online_count ? '<ul class="inline-list c2">'.$online_list.'</ul>' : '' ).'

<ul class="plain-list c1" style="clear:both">
<li><a href="prefs" title="Click here to change settings">Current time</a> is '.date2 (time()),"</li>\n";

$tmp = mysql_query('SELECT UNIX_TIMESTAMP(`time`), `messages`.`topic`, `board`, `title`, `messages`.`user`
		FROM `messages`
		LEFT JOIN `topics` USING (`topic`)
		WHERE `messages`.`visible` >= 0
		ORDER BY `message` DESC LIMIT 1');

if ( mysql_num_rows($tmp) ) {
	$lastmsg = mysql_fetch_row($tmp);
	echo '<li><a href="viewtopic?b=',$lastmsg[2],';t=',$lastmsg[1],
		'" accesskey="p" title="Go to most recent post">Last Post</a> was at ',date2($lastmsg[0]),"</li>\n";
}

mysql_free_result($tmp);

echo '</ul>';

footer();
?>