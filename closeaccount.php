<?php
require 'config.php';
$require_login = true;
$page_name = 'Close User Account';
$level_restriction = PENDING_CLOSE;
require 'top.inc.php';
require 'encryption.inc.php';

if ( isset($_POST['submit']) ) {
	list($ip) = mysql_fetch_row(mysql_query('SELECT INET_NTOA(`register_ip`) AS `ip` FROM `users`
			WHERE `user` = '.$userinfo['user']));
	if ( $_POST['submit'] == 'Close Account' ) {
		if ( $userinfo['password'] != encrypt($_POST['enterpass'], $ip) )
			echo '<div class="alert">Your current password does not match the one entered.</div>',"\n";
		elseif ( $_POST['enterpass'] != $_POST['fux'] )
			echo '<div class="alert">The passwords do not match.</div>',"\n";
		else {
			mysql_query('UPDATE `users` SET `level` = '.PENDING_CLOSE.'
					WHERE `user` = '.$userinfo['user'].'
					AND `password` = \''.encrypt($_POST['enterpass'], $ip).'\' LIMIT 1');
			stop('Your account is now pending closure and will be permanently closed after 24 hours with no login.');
		}
	}
	elseif ( $_POST['submit'] == 'Unclose Account' ) {
		mysql_query('UPDATE `users` SET `level` = '.NEW_USER.' WHERE `userid` = '.$userinfo['user'].' LIMIT 1');
		stop('Your account has now been reopened at level '.NEW_USER.'.');
	}
}

if ( $userinfo['level'] >= NEW_USER )
	echo '<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">
<div class="alert">Warning: After an account is closed it cannot be reopened.</div>
<dl class=',colour(),'>
<dt>Confirm current password</dt>
<dd><input type="password" name="enterpass"/></dd>
<dt>Again</dt>
<dd><input type="password" name="fux"/></dd>
<dt>Then click here</dt>
<dd><input type="submit" name="submit" value="Close Account"/></dd>
</dl>
</form>
';
else
	echo '<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">
<div class=',colour(),'>
<input type="submit" name="submit" value="Unclose Account"/>
</div>
</form>
';

require 'foot.php';
?>