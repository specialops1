<?php
require 'config.php';
$require_login = true;
$page_name = 'User Information';
require 'top.inc.php';
include 'include/levels.php';

$msgs = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages` WHERE `user` = '.$userinfo['user'].' AND `visible` >= 0'), 0);
$modups = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages` WHERE `user` = '.$userinfo['user'].' AND `score` > 0'), 0);
$moddowns = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages` WHERE `user` = '.$userinfo['user'].' AND `score` < 0'), 0);
$topics = mysql_result(mysql_query('SELECT COUNT(*) FROM `topics` WHERE `user` = '.$userinfo['user']), 0);

echo '<table>
<tr class=',colour(),'><td>User ID</td><td colspan="2">',$userinfo['user'],'</td></tr>
<tr class=',colour(),'><td>User level</td><td colspan="2">',$cfg['leveldesc'][$userinfo['level']],'</td></tr>
<tr class=',colour(),'><td>Messages</td><td><a href="post-history',URL_STRING,'">',$msgs,' active</a> / ',$moddowns,' modded / ',$modups,' suggested</td></tr>
<tr class=',colour(),'><td>Topics</td><td>',$topics,'</td></tr>
<tr class=',colour(),'><td>',$strings['cookies'],'</td><td colspan="2">',$userinfo['cookies'],'</td></tr>
<tr class=',colour(),'><td>',$strings['points'],'</td><td colspan="2">',$userinfo['points'],'</td></tr>
<tr class=',colour(),'><td>Date registered</td><td colspan="2">',$userinfo['register_date'],'; account age (days): ',floor((time() - strtotime($userinfo['register_date'])) / 86400),'</td></tr>
<tr class=',colour(),'><td>Private email address</td><td>',$userinfo['private_email'],'</td></tr>
<tr class=',colour(),'><td>Public email address</td><td>',$userinfo['public_email'],'</td></tr>
<tr class=',colour(),'><td>Signature</td><td>',nl2br($userinfo['sig']),'</td></tr>
<tr class=',colour(),'><td>Quote</td><td>',$userinfo['quote'],'</td></tr>
<tr class=',colour(),'><td>Browser</td><td colspan="2">',$userinfo['useragent'],'</td></tr>
</table>

<div class="c3">Other options</div>
<ul class="plain-list">
<li style="float:right; clear:right"><a href="prefs',URL_STRING,'">Change settings</a></li>
<li style="float:right; clear:right"><a href="theme',URL_STRING,'">Change theme</a></li>
<li style="float:right; clear:right"><a href="changepassword',URL_STRING,'">Change password</a></li>
<li><a href="stuff',URL_STRING,'">Statistics/FAQ</a></li>
<li><a href="userdir?list=banned'.URL_APPEND.'">Banned/suspended users</a></li>
<li><a href="userdir',URL_STRING,'">User directory</a></li>',"\n";

if ( $userinfo['level'] >= NEW_USER )
	echo '<li><a href="closeaccount',URL_STRING,'">Close account</a></li>',"\n";
elseif ( $userinfo['level'] == PENDING_CLOSE )
	echo '<li><a href="closeaccount',URL_STRING,'">Reopen account</a></li>',"\n";

echo "</ul>\n";

footer();
?>
