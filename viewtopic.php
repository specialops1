<?php
define('CSS_INC', 'topic');
require 'config.php';

if ( isset($_GET['t']) ) {
	$topicinfo = mysql_fetch_row(mysql_query('SELECT `title`, `visible`, `board` FROM `topics`
		WHERE `topic` = '.intval($_GET['t'])));
	if ( $topicinfo )
		list($page_name, $closed, $board) = $topicinfo;
	$page_name = 'Message list: '.$page_name;

	if ( isset($board) ) {
		$_GET['b'] = $board;
		$boardinfo = mysql_fetch_row(mysql_query('SELECT `view_level`, `post_level` FROM `boards`
			WHERE `board` = '.$board));
		if ( $boardinfo )
			list($level_restriction, $topic_level) = $boardinfo;
	}
}

require 'top.inc.php';

if ( empty($board) )
	printf('Invalid board ID.') && footer();
elseif ( $userinfo['level'] < MOD && $topicinfo[1] < -1 )
	printf('<div class="alert">This topic has been deleted.</div>') && footer();
elseif ( !mysql_result(mysql_query('SELECT COUNT(*) FROM `messages` WHERE `topic` = '.intval($_GET['t'])), 0) )
	printf('<div class="alert">Invalid link.</div>') && footer();

if ( $topicinfo[1] < 0 )
	echo '<div class="alert">This topic is closed.</div>',"\n";

$page = isset($_GET['p']) ? intval($_GET['p']) : 1;
$pages = ($page-1) * $userinfo['msgs_page'];
$msg_count = $pages;

$number_of_items = mysql_result(mysql_query('SELECT COUNT(*) FROM `messages`
		WHERE `topic` = '.intval($_GET['t']).
		( $userinfo['level'] < MOD ? ' AND `visible` >= 0' : '' ).
		' ORDER BY `time` ASC, `message` ASC'), 0);

if ( $number_of_items > $userinfo['msgs_page'] ) {
	$count = 0;
	$pagelist = '<div class="c3">Page '.$page." | \n";
	while ($count < $number_of_items / $userinfo['msgs_page'])
		$pagelist .= '<a href="?p='.++$count.URL_APPEND.'">'.$count."</a>\n";
	$pagelist .= '</div>';
}
else
	$pagelist = '';

echo $pagelist;

$message_list = mysql_query('SELECT `messages`.`message`, `user`, UNIX_TIMESTAMP(`time`), `score`, `content`, `topic`, INET_NTOA(`ip`), `visible`
	FROM `messages` LEFT JOIN `message-data` USING (`message`)
	WHERE `topic` = '.intval($_GET['t']).
	( $userinfo['level'] < MOD ? ' AND `visible` >= 0' : '' ).
	' ORDER BY `time` ASC, `message` ASC
	LIMIT '.$pages.', '.$userinfo['msgs_page']);

while ( $message = mysql_fetch_row($message_list) ) {
	echo '<div class="c2" id="m',++$msg_count,'">From: ',userlink($message[1]),' (',mysql_result(mysql_query('SELECT `level` FROM `users` WHERE `user` = '.$message[1].' LIMIT 1'), 0),') | Posted: ',date2($message[2]),
	( isset($userinfo['user']) ? ' | <a href="messagedetail?m='.$message[0].URL_APPEND.'">Options</a>' : '' );
	if ( $message[3] < 0 )
		echo ' | ',$cfg['img'][1],' ',$message[3],' ',$strings['cookies'];
	elseif ( $message[3] > 0 )
		echo ' | ',$cfg['img'][0],' ',$message[3],' ',$strings['cookies'];
	if ( $userinfo['level'] >= MOD ) {
		echo ' | <a href="ipinfo?ip=',$message[6],URL_APPEND,'">',$message[6],'</a>';
		if ( $message[7] < 0 )
			echo ' | ',$cfg['img'][3];
	}
	echo ' | <a href="#m',$msg_count,'">#',$msg_count,"</a></div>\n",
		'<div class="c1" style="margin:2px">';
	echo $message[4],"</div>\n";
}

echo $pagelist;

if ( $userinfo['level'] >= ADMIN )
	echo '<div class="c3"><a href="edittopic',URL_STRING,'">Topic Options</a></div>';

footer();
?>
