<?php
ob_start(); // This is cheating
require 'config.php';

$require_login = true;
$page_name = 'Change Password';
$level_restriction = NEW_USER;
require 'top.inc.php';
require 'encryption.inc.php';

if (isset ($_POST['submit']))
{
	if ( $userinfo['password'] != encrypt($_POST['old_pw'], $userinfo['register_ip']) )
		echo '<div class="alert">Your current password does not match the one entered.</div>';
	elseif ( $_POST['new_pw'] != $_POST['confirm_pw'] )
		echo '<div class="alert">Your new passwords do not match.</div>';
	else {
		mysql_query('UPDATE `users` SET `password` = \''.mysql_real_escape_string(encrypt($_POST['new_pw'], $userinfo['register_ip'])).'\' WHERE
`username` = \''.mysql_real_escape_string ($userinfo['username']).'\' AND
`password` = \''.mysql_real_escape_string (encrypt ($_POST['old_pw'], $userinfo['regip'])).'\' LIMIT 1');
		setcookie('password', encrypt ($_POST['new_pw'], $userinfo['regip']), time()+7776000);
		stop('Password successfully changed.');
	}
}

echo '
<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">
<dl class=',colour(),'>
<dt>Current Password</dt>
<dd><input type="password" maxlength="40" size="40" name="old_pw"/></dd>
<dt>New Password</dt>
<dd><input type="password" maxlength="40" size="40" name="new_pw"/></dd>
<dt>Confirm new password</dt>
<dd><input type="password" maxlength="40" size="40" name="confirm_pw"/></dd>
</dl>
<input type="submit" name="submit" value="Change"/>
</form>
';

require ('foot.php');
?>