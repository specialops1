<?php
require ('config.php');
$level_restriction = ADMIN2;
$require_login = true;
$page_name = 'Level Editor';
require ('top.inc.php');

if (isset ($_POST['submit']))
{
	$nochange = isset ($_POST['nochange']) ? 1 : 0;
	$old_level = intval ($_POST['old_level']);
	$new_level = intval ($_POST['new_level']);
	if ($_POST['submit'] == 'Edit Level' && $old_level != $new_level)
	{
		echo '<div class="alert">';
		if (mysql_result (mysql_query ('SELECT COUNT(*) FROM `levels` WHERE `levnum` = '.$old_level), 0) > 0)
			echo 'Error: Level number already in use.';
		else
		{
			mysql_query ('UPDATE `users` SET `level` = '.$new_level.' WHERE `level` = '.$old_level);
			$users = mysql_affected_rows ();
			$boards = mysql_result (mysql_query ('SELECT COUNT(*) FROM `boards` WHERE
`boardlevel` = '.$old_level.' OR `topiclevel` = '.$old_level.' OR `postlevel` = '.$old_level), 0);
			mysql_query ('UPDATE `boards` SET `boardlevel` = '.$new_level.' WHERE `boardlevel` = '.$old_level);
			mysql_query ('UPDATE `boards` SET `topiclevel` = '.$new_level.' WHERE `topiclevel` = '.$old_level);
			mysql_query ('UPDATE `boards` SET `postlevel` = '.$new_level.' WHERE `postlevel` = '.$old_level);
			mysql_query ('UPDATE `levels` SET
`levdefine` = \''.mysql_real_escape_string ($_POST['levdefine']).'\',
`levnum` = '.intval ($_POST['new_level']).',
`levname` = \''.mysql_real_escape_string ($_POST['levname']).'\',
`caption` = \''.mysql_real_escape_string ($_POST['caption']).'\',
`minaura` = '.intval ($_POST['minaura']).',
`nochange` = '.$nochange.'
WHERE `levnum` = '.$old_level);
			echo 'Level edited: '.$old_level.' to '.$new_level.' ('.$users.' users affected, '.$boards.' boards affected)';
		}
		echo '</div>';
	}
	elseif ($_POST['submit'] == 'Edit Level')
	{
		mysql_query ('UPDATE `levels` SET
`levdefine` = \''.mysql_real_escape_string ($_POST['levdefine']).'\',
`levname` = \''.mysql_real_escape_string ($_POST['levname']).'\',
`caption` = \''.mysql_real_escape_string ($_POST['caption']).'\',
`minaura` = '.intval ($_POST['minaura']).',
`nochange` = '.$nochange.'
WHERE `levnum` = '.$old_level);
		echo '<div class="alert">Level edited.</div>';
	}
}

echo '
<div class="c3">Manage Levels</div>
<table>
<tr>
<th>Level #, Name, Caption</th>
<th>Definition</th>
<th>Minimum '.$cfg['bds']['aura'].'</th>
<th>Fixed Level</th>
<th>Edit</th>
</tr>
';

$result = mysql_query ('SELECT * FROM `levels` ORDER BY `levnum` DESC');
while ($myrow = mysql_fetch_assoc ($result))
{
	if (isset ($_GET['editlevel']) && intval ($_GET['lid']) == $myrow['levnum'])
	{
		$myrow = mysql_fetch_assoc (mysql_query ('SELECT * FROM `levels` WHERE `levnum` = '.intval ($_GET['lid'])));
		echo '<tr class="alert"><td colspan="5">
<form method="post" action="'.urlpath(2).'">
<input type="hidden" name="old_level" value="'.$myrow['levnum'].'"/>
<fieldset style="display:inline;height:6em">
<legend>Level Number, Name &amp; Caption</legend>
<input type="text" name="new_level" style="width:2em" value="'.$myrow['levnum'].'"/>
<input type="text" name="levname" maxlength="12" value="'.$myrow['levname'].'"/><br />
<textarea name="caption">'.$myrow['caption'].'</textarea>
</fieldset>

<fieldset style="display:inline;height:6em">
<legend>Other</legend>
Level Definition: <input type="text" name="levdefine" maxlength="15" value="'.$myrow['levdefine'].'"/><br/>
Minimum '.$cfg['bds']['aura'].': <input type="text" name="minaura" size="3" value="'.$myrow['minaura'].'"/><br/>
<label for="fixed_level">Fixed Level:</label>
<input type="checkbox" id="fixed_level" name="nochange'.($myrow['nochange'] ? '" checked="checked' : '').'"/><br/>
<input type="submit" name="submit" value="Edit Level"/>
<input type="submit" value="Cancel"/>
</fieldset>
</form>
</td></tr>
';
	}
	else
		echo '<tr class='.colour().'><td>
<b>'.$myrow['levnum'].': '.$myrow['levname'].'</b><br/>
<small>'.$myrow['caption'].'</small></td>
<td>'.$myrow['levdefine'].'</td>
<td>'.$myrow['minaura'].'</td>
<td>'.($myrow['nochange'] ? 'Yes' : 'No').'</td>
<td><a href="?editlevel=1;lid='.$myrow['levnum'].urlpath(1).'">Edit</a></td></tr>
';
}

echo '</table>';

require ('foot.php');
?>