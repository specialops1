<?php
require ('config.php');
require ('extfuncs.inc.php');
$level_restriction = INACTIVE_USER;
$require_login = true;

if (
	isset ($_POST['submit'])
	&& (strlen ($_POST['email']) <= 50)
	&& (strlen ($_POST['email2']) <= 50)
	&& (strlen ($_POST['im']) <= 50)
	&& (strlen ($_POST['sig']) <= 255)
	&& (strlen ($_POST['quote']) <= 255)
)
{
	$quote = ($userinfo['level'] < ADMIN ? htmlentities ($_POST['quote']) : nl2br ($_POST['quote']));
	$logintimer = ($_POST['logintimer'] >= 60 && $_POST['logintimer'] <= 3600) ? intval ($_POST['logintimer']) : 10;
	$ppp = ($_POST['ppp'] >= 5 && $_POST['ppp'] <= 150) ? intval ($_POST['ppp']) : 35;
	$tpp = ($_POST['tpp'] >= 5 && $_POST['tpp'] <= 100) ? intval ($_POST['tpp']) : 35;
	
	foreach (array ('postlayout', 'timezone', 'hidepoll') as $varname)
		$$varname = intval ($_POST[$varname]);
	foreach (array ('sig', 'quote', 'email', 'email2', 'im', 'timeformat') as $varname)
		$$varname = mysql_real_escape_string ($_POST[$varname]);
	
	$prefs = ($userinfo['viewdisp'] | 0x17f) - 0x17f;	//(127+256)
	$prefs += (int) isset ($_POST['topic_views']);
	$prefs += (int) isset ($_POST['view_time']) * 2;
	$prefs += (int) isset ($_POST['showonline']) * 4;
	$prefs += (int) isset ($_POST['altstyles']) * 8;
	$prefs += (int) isset ($_POST['dst']) * 16;
	$prefs += intval ($_POST['postlayout']) * 32;
	$prefs += (int) isset ($_POST['css_special']) * 64;
	$prefs += (int) (isset ($_POST['invisible']) && $userinfo['level'] >= ELI_USER) * 256;
	
	mysql_query ('UPDATE `users` SET
		`email` = \''.htmlentities ($email).'\',
		`email2` = \''.htmlentities ($email2).'\',
		`AIM` = \''.htmlentities ($im).'\',
		`sig` = \''.htmlentities ($sig).'\',
		`quote` = \''.$quote.'\',
		`viewdisp` = '.$prefs.',
		`timezone` = '.$timezone.',
		`timeformat` = \''.$timeformat.'\',
		`logintimer` = '.$logintimer.',
		`hidepoll` = '.$hidepoll.',
		`ppp` = '.$ppp.',
		`tpp` = '.$tpp.'
		WHERE `userid` = '.$userinfo['userid'].' LIMIT 1'
	);
	$userinfo = mysql_fetch_assoc (mysql_query ('SELECT * FROM `users` WHERE `userid` = '.$userinfo['userid']));
	$updated = true;
	$userinfo['timeoffset'] = ($userinfo['timezone'] + (bool) ($userinfo['viewdisp'] & 16)) * 3600;
}

$page_name = 'User Settings';
require ('top.inc.php');

if (isset ($updated))
	echo '<div class="alert">Preferences updated.</div>
';

$userinfo['quote'] = str_replace ('<br />', '', $userinfo['quote']);

echo '<form method="post" action="'.urlpath(2).'">
<h2>Display Options</h2>
<dl>
<dt>Topic list</dt>
<dd>Show <input type="text" name="tpp" value="'.$userinfo['tpp'].'" maxlength="3" size="3"/> topics per page (5-100)</dd>

<dd><input type="checkbox" name="topic_views"'.($userinfo['viewdisp'] & 1 ? ' checked="checked"' : '').' id="topic_views"/>
<label for="topic_views">Show number of views for each topic</label></dd>

<dd><input type="checkbox" name="view_time"'.($userinfo['viewdisp'] & 2 ? ' checked="checked"' : '').' id="view_time"/>
<label for="view_time">Show date of last view for each topic</label></dd>


<dt>Message list</dt>
<dd>Show <input type="text" name="ppp" value="'.$userinfo['ppp'].'" maxlength="3" size="3"/> posts per page (5-150)</dd>


<dt>Post page (buttons)</dt>
<dd><input type="radio" name="postlayout"'.($userinfo['viewdisp'] & 32 ? '' : ' checked="checked"').' id="postlayout1" value="0"/>
<label for="postlayout1">[Post] [Preview]</label></dd>

<dd><input type="radio" name="postlayout"'.($userinfo['viewdisp'] & 32 ? ' checked="checked"' : '').' id="postlayout2" value="1"/>
<label for="postlayout2">[Preview] [Post]</label></dd>


<dt>Poll</dt>
<dd><input type="radio" name="hidepoll"'.($userinfo['hidepoll'] == 0 ? ' checked="checked"' : '').' id="hide_poll" value="0"/>
<label for="hide_poll">Always hide poll</label></dd>

<dd><input type="radio" name="hidepoll"'.($userinfo['hidepoll'] == 1 ? ' checked="checked"' : '').' id="show_poll" value="1"/>
<label for="show_poll">Always show poll</label></dd>

<dd><input type="radio" name="hidepoll"'.($userinfo['hidepoll'] == 2 ? ' checked="checked"' : '').' id="autohide_poll" value="2"/>
<label for="autohide_poll">Hide if already voted</label></dd>


<dt>Time display</dt>
<dd>Timezone offset: <select name="timezone">';
for ($hour = -12; $hour <= 13; $hour++)
	echo '<option value="'.$hour.'"'.($userinfo['timezone'] == $hour ? ' selected="selected"' : '').'>GMT '.($hour > 0 ? '+' : '').($hour ? $hour : '').'</option>'."\n";
echo '</select></dd>

<dd><input type="checkbox" name="dst"'.($userinfo['viewdisp'] & 16 ? ' checked="checked"' : '').' id="dst"/>
<label for="dst">Adjust time automatically for DST/BST</label></dd>

<dd>Displayed <a href="http://php.net/date">date/time format</a>:
<input type="text" name="timeformat" value="'.$userinfo['timeformat'].'" maxlength="30"/></dd>

<dd>Current settings: '.date2 (time()).'</dd>


<dt>Other</dt>
<dd>Show users as online for
<input type="text" name="logintimer" value="'.$userinfo['logintimer'].'" maxlength="4" style="width:4em"/> seconds idle (60-3600)</dd>

<dd><input type="checkbox" name="showonline"'.($userinfo['viewdisp'] & 4 ? ' checked="checked"' : '').' id="showonline"/>
<label for="showonline">Show online users in menubar</label></dd>

<dd><input type="checkbox" name="altstyles"'.($userinfo['viewdisp'] & 8 ? ' checked="checked"' : '').' id="altstyles"/>
<label for="altstyles">Enable alternate CSS</label></dd>

<dd><input type="checkbox" name="css_special"'.($userinfo['viewdisp'] & 64 ? ' checked="checked"' : '').' id="css_special"/>
<label for="css_special">Enable extended CSS</label></dd>

'.($userinfo['level'] >= ELI_USER ? '<dd><input type="checkbox" name="invisible"'.($userinfo['viewdisp'] & 256 ? ' checked="checked"' : '').' id="invisible"/>
<label for="invisible">Invisible login mode</label></dd>' : '').'
</dl>


<h2>Profile</h2>
<ul class="plain-list">
<li>Private Email (not publicly displayed, max 50 chars): <input type="text" name="email" value="'.$userinfo['email'].'" maxlength="50"/></li>

<li>Public Email (visible to logged in users, max 50 chars): <input type="text" name="email2" value="'.$userinfo['email2'].'" maxlength="50"/></li>

<li>IM screenname (visible to logged in users, max 50 chars): <input type="text" name="im" value="'.$userinfo['AIM'].'" maxlength="50"/></li>

<li>Signature (max 255 chars): <textarea name="sig" rows="4" cols="80" style="width: 100%">'.$userinfo['sig'].'</textarea></li>

<li>Quote (shown in userinfo, max 255 chars): <textarea name="quote" rows="4" cols="80" style="width: 100%">'.$userinfo['quote'].'</textarea></li>
</ul>


<div class="c3"><input type="submit" name="submit" value="Save Settings" accesskey="s"/> <small>(Alt+S)</small></div>
</form>';

require ('foot.php');
?>