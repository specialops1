<?php
/* FIXME OR DIE */

//get page title
empty ($title_name) ?
	($page_name ?
		$pagetitle = $page_name.' - '.$strings['name']
		: $pagetitle = $strings['name'])
	:
	($pagetitle = $title_name.' - '.$strings['name']);

define('FILENAME', basename($_SERVER['PHP_SELF']));

//header('Content-Type: application/xhtml+xml; charset=UTF-8');
header('Content-Type: text/html; charset=UTF-8');
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">'."\n";

//output html headers
echo '<head>
<title>',strip_tags($pagetitle),'</title>
<link href="csslib/default.css" rel="stylesheet"/>',"\n";

//extra css thing [FIXME: make this an array]
if ( defined('CSS_INC') )
	echo '<link href="csslib/'.CSS_INC.'" rel="stylesheet"/>'."\n";

//theme page javascript thing
if ( strpos(FILENAME, 'theme') === 0 )
	echo '<script src="include/themepage.js" type="text/javascript"></script>'."\n";

if ( empty($userinfo['user']) )
	echo '<link href="csslib/system/default" type="text/css" rel="stylesheet"/>';
else {
	if ( $userinfo['theme'] > 0 ) {
		$cssinfo = mysql_fetch_row(mysql_unbuffered_query('SELECT `file`, `title` FROM `styles`
				WHERE `style` = \''.$userinfo['theme'].'\' LIMIT 1'));
		echo '<link href="csslib/system/',$cssinfo[0],'" type="text/css" title="',$cssinfo[1],'" rel="stylesheet"/>'."\n";
	}
	elseif ( $userinfo['theme'] === '0' )
		echo '<link href="csslib/user/',$userinfo['user'],'" type="text/css" title="User Stylesheet" rel="stylesheet"/>',"\n";
}

echo '</head>

<body id="so_boards">
<h1>',$cfg['headers2'][$userinfo['header']],"</h1>\n";

// Page title & subtitle
if ( $page_name )
	echo '<h2>',$page_name,"</h2>\n";

if ( strpos(FILENAME, 'topic') === 0 )
	echo '<h3>',$topicinfo[0],"</h3>\n";

// Menu bar
echo '<ul id="menubar">'."\n";
if ( isset($userinfo['user']) )
	echo '<li><a href="user',URL_STRING,'" accesskey="u">',$userinfo['name'],' (',$userinfo['level'],")</a></li>\n";
else
	echo '<li><a href="login',URL_STRING,'" accesskey="l">Log in</a></li>
<li><a href="register',URL_STRING,'" accesskey="r">Register</a></li>',"\n";

if ( strpos(FILENAME, 'index') === false )
	echo '<li><a href="." accesskey="b" rel="top">Board List</a></li>',"\n";

if ( isset($_GET['b']) ) {
	if ( strpos(FILENAME, 'board') === false )
		echo '<li><a href="viewboard?b=',intval($_GET['b']),'" accesskey="t" rel="contents">Topic List</a></li>',"\n";
	elseif ( isset($userinfo['user']) && strpos(FILENAME, 'viewboard') === 0 &&
			$userinfo['level'] >= $topic_level && empty($_GET['t']) )
		echo '<li><a href="post?b=',intval($_GET['b']),'" accesskey="t">New Topic</a></li>',"\n";

	if ( isset($_GET['t']) )
		if ( strpos(FILENAME, 'viewtopic') !== false && isset($userinfo['name']) &&
				$closed >= 0 && $userinfo['level'] >= $topic_level )
			echo '<li><a href="post',URL_STRING,'" accesskey="m">New Message</a></li>',"\n";
		else
			echo '<li><a href="viewtopic',URL_STRING,'" accesskey="m" rel="up">Message List</a></li>',"\n";
}

if ( isset($userinfo['user']) ) {
	echo '<li><a href="logout',URL_STRING,'" accesskey="l">Log Out</a></li>'."\n";

	if ( $userinfo['level'] >= MOD ) {
		$marks = mysql_result(mysql_query('SELECT COUNT(*) FROM `marks` WHERE `actioned` IS NULL'), 0);
		echo '<li><a href="markqueue',URL_STRING,'" accesskey="q">Queue: ',$marks,"</a></li>\n",
			'<li><a href="modresources',URL_STRING,'" accesskey="r">Mod Resources</a></li>',"\n";
	}
}

// End of menu, start of page
echo '</ul>
<div id="content">',"\n";

$tmp = null;
if ( isset($require_login) && $require_login === true && empty($userinfo['user']) )
	$tmp = 'You must be <a href="login'.URL_STRING.'">Logged In</a> to view this page.';
elseif ( isset($require_login) && $require_login === false && isset($userinfo['user']) )
	$tmp = 'You must be <a href="logout'.URL_STRING.'">Logged Out</a> to view this page.';
elseif ( isset($level_restriction) && $userinfo['level'] < $level_restriction )
	$tmp = 'You cannot view this page. This page is restricted to users level '.$level_restriction.' and above.';

if ( $tmp ) {
	header('HTTP/1.1 403 Forbidden');
	echo '<div class="alert">',$tmp,'</div>';
	footer();
}

unset($tmp);
?>
