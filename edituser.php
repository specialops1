<?php
require 'config.php';
$level_restriction = ADMIN;
$require_login = true;
$page_name = 'Edit User';
require 'top.inc.php';
include 'include/levels.php';

if ( isset($_POST['submit']) && strpos($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']) === 0 ) // prevent remote form submitting
{
	mysql_query('UPDATE `users` SET
		`level` = '.intval($_POST['levlev']).',
		`cookies` = '.intval($_POST['cookies']).',
		`points` = '.intval($_POST['points']).',
		`sig` = \''.mysql_real_escape_string($_POST['sig']).'\',
		`quote` = \''.mysql_real_escape_string($_POST['quote']).'\',
		`public_email` = \''.mysql_real_escape_string($_POST['public_email']).'\'
		WHERE `user` = '.intval($_GET['user']).' LIMIT 1');

	if ( isset($_POST['clear_email']) )
		mysql_query ('UPDATE `users` SET `private_email` = \'\' WHERE `user` = '.intval ($_GET['user']).' LIMIT 1');

	echo '<p class="alert">User updated.</p>';
}

$user2 = mysql_fetch_assoc(mysql_query('SELECT * FROM `users` WHERE `user` = '.intval($_GET['user'])));

if ( !$user2 )
	print('<p class="alert">Invalid user ID.</p>') && footer();
elseif ( $user2['level'] > $userinfo['level'] || $user2['user'] == 1 )
	print('<p class="alert">You cannot edit this user\'s info.</p>') && footer();

echo '<form method="post" action="?user=',intval($_GET['user']),URL_APPEND,'">
<h3>Editing info for ',userlink($user2['user']),' (User ID ',$user2['user'],')</h3>

<table>
<tr class=',colour(),'><th scope="col">User Level</th><td><select name="levlev">',"\n";

foreach ( $cfg['levels'] as $num => $nam )
	echo '<option value="',$num,'"',( $user2['level'] == $num ? ' selected="selected"' : '' ),'>',$num,': ',$nam,"</option>\n";
echo '</select></td></tr>

<tr class=',colour(),'><th scope="col">',$strings['cookies'].'</th>
	<td><input type="text" name="cookies" value="'.$user2['cookies'].'"/></td></tr>
<tr class=',colour(),'><th scope="col">',$strings['points'].'</th>
	<td><input type="text" name="points" value="'.$user2['points'].'"/></td></tr>
<tr class=',colour(),'><th scope="col">Private Email</th>
	<td>'.htmlentities($user2['private_email']).' | <label><small>Clear </small><input type="checkbox" name="clear_email"/></label></td></tr>
<tr class=',colour(),'><th scope="col">Public Email</th>
	<td><input type="text" name="public_email" value="'.htmlspecialchars($user2['public_email']).'"/></td></tr>
<tr class=',colour(),'><th scope="col">Signature</th>
	<td><textarea cols="40" rows="4" name="sig">',htmlspecialchars($user2['sig']),'</textarea></td></tr>
<tr class=',colour(),'><th scope="col">Quote</th>
	<td><textarea cols="40" rows="4" name="quote">',htmlspecialchars(str_replace('<br />', '', $user2['quote'])),'</textarea></td></tr>
</table>
<p class="c3"><input type="submit" name="submit" value="Update"/></p>
</form>',"\n";

footer();
?>