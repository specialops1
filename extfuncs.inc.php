<?php
function check_ua()
{
	$uas = array (
		'AOL',
		'MSIE'
	);
	foreach ($uas as $blocked_ua)
		if (strpos ($_SERVER['HTTP_USER_AGENT'], $blocked_ua) !== false)
			return $blocked_ua;
}

function check_isp()
{
	$hostname = gethostbyaddr ($_SERVER['REMOTE_ADDR']);
	$isps = file ('include/isp_bans.txt');
	foreach ($isps as $blocked_isp)
		if (strpos ($hostname, $blocked_isp) !== false)
			return true;
}

function check_email ($email)
{
	$emails = file ('include/email_bans.txt');
	foreach ($emails as $blocked_email)
		if (strpos ($email, rtrim ($blocked_email)) !== false)
			return true;
}

function check_ip()
{
	$isp = gethostbyaddr ($_SERVER['REMOTE_ADDR']);
	if (strpos ($isp, 'ntli.net') || strpos ($isp, 'ntl.com')) // NTL is a retarded ISP
		return false;
	$ips = file ('compress.zlib://include/proxylist.txt.gz');
	foreach ($ips as $blocked_ip)
		if (strpos ($_SERVER['REMOTE_ADDR'], rtrim ($blocked_ip)) === 0)
			return true;
}
?>