<?php
require 'config.php';
require 'extfuncs.inc.php';
$level_restriction = INACTIVE_USER;
$require_login = true;

if (
	isset ($_POST['submit'], $_POST['public_email'], $_POST['private_email'], $_POST['sig'], $_POST['quote'])
	&& (strlen($_POST['public_email']) <= 50)
	&& (strlen($_POST['private_email']) <= 50)
	&& (strlen($_POST['sig']) <= 255)
	&& (strlen($_POST['quote']) <= 255)
) {
	$msgs_page = $_POST['msgs_page'] >= 5 && $_POST['msgs_page'] <= 150 ? intval($_POST['msgs_page']) : 35;
	$topics_page = $_POST['topics_page'] >= 5 && $_POST['topics_page'] <= 150 ? intval($_POST['topics_page']) : 35;

	mysql_query ('UPDATE `users` SET
		`public_email` = \''.mysql_real_escape_string(htmlspecialchars($_POST['public_email'])).'\',
		`private_email` = \''.mysql_real_escape_string(htmlspecialchars($_POST['private_email'])).'\',
		`sig` = \''.mysql_real_escape_string(htmlspecialchars($_POST['sig'])).'\',
		`quote` = \''.mysql_real_escape_string(htmlspecialchars($_POST['quote'])).'\',
		`dateformat` = \''.mysql_real_escape_string($_POST['dateformat']).'\',
		`timezone` = '.intval($_POST['timezone']).',
		`msgs_page` = '.$msgs_page.',
		`topics_page` = '.$topics_page.'
		WHERE `user` = '.$userinfo['user'].' LIMIT 1'
	);
	$userinfo = mysql_fetch_assoc(mysql_query('SELECT * FROM `users` WHERE `user` = '.$userinfo['user']));
	$updated = true;
}

$page_name = 'User Settings';
require 'top.inc.php';

if ( isset($updated) )
	echo '<div class="alert">Preferences updated.</div>',"\n";

$userinfo['quote'] = str_replace('<br />', '', $userinfo['quote']);

echo '<form method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">
<table>
<caption><h3>Display Options</h3></caption>
<thead><tr><th>Setting</th><th>Value</th></tr></thead>
<tbody>
<tr><td>Topics per page</td>
<td>Show <input type="text" name="topics_page" value="'.$userinfo['topics_page'].'" size="3"/> (5-100)</td></tr>
<tr><td>Messages per page</td>
<td>Show <input type="text" name="msgs_page" value="'.$userinfo['msgs_page'].'" size="3"/> (5-150)</td></tr>

<tr><td>Timezone offset</td>
<td><select name="timezone">',"\n";
for ($hour = -12; $hour <= 13; $hour++)
	echo "\t",'<option value="',$hour,'"',( $userinfo['timezone'] == $hour ? ' selected="selected"' : '' ),'>',
			( $hour > 0 ? '+' : '' ),$hour,"</option>\n";
echo '</select></td></tr>

<tr><td>Displayed <a href="http://php.net/date">date/time format</a><br/>
<small>(Leave blank for default)</small></td>
<td><input type="text" name="dateformat" value="',$userinfo['dateformat'],'" maxlength="30"/><br/>
<small>Date/time according to current settings: ',date2(time()),'</small></td></tr>
</tbody>
</table>

<table>
<caption><h3>Profile</h3></caption>
<thead><tr><th>Setting</th><th>Value</th></tr></thead>
<tbody>
<tr><td>Private email<br/>
<small>(Not publicly displayed, max 50 chars)</small></td>
<td><input type="text" name="private_email" value="',$userinfo['private_email'],'" maxlength="50"/></td></tr>
<tr><td>Public Email<br/>
<small>(visible to logged in users, max 50 chars)</small></td>
<td><input type="text" name="public_email" value="',$userinfo['public_email'],'" maxlength="50"/></td></tr>
<tr><td>Signature<br/>
<small>(max 255 chars)</small></td>
<td><textarea name="sig" rows="4" cols="80" style="width: 100%">',$userinfo['sig'],'</textarea></td></tr>

<tr><td>Quote<br/>
<small>(shown in userinfo, max 255 chars)</td>
<td><textarea name="quote" rows="4" cols="80" style="width:100%">',
	str_replace('<br />', '', $userinfo['quote']),'</textarea></td></tr>
</tbody>
</table>

<p class="c3"><input type="submit" name="submit" value="Save Settings (S)" accesskey="s"/></p>
</form>';

footer();
?>