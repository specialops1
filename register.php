<?php
$page_name = 'Registration';
$require_login = false;
require 'config.php';
require 'top.inc.php';
require 'extfuncs.inc.php';
require 'encryption.inc.php';

if ( isset($_GET['regkey']) ) {
	$result = mysql_query('SELECT COUNT(*) FROM `users`
			WHERE MD5(CONCAT(`name`, `user`)) = \''.mysql_real_escape_string($_GET['regkey']).'\'');
	if ( mysql_result($result, 0) ) {
		mysql_query('UPDATE `users` SET `level` = '.INACTIVE_USER.'
				WHERE `level` = '.PENDING_EMAIL.'
				AND MD5(CONCAT(`name`, `user`)) = \''.mysql_real_escape_string($_GET['regkey']).'\'');
		stop('<p>Your username has been successfully registered with an account level of '.INACTIVE_USER.".</p>\n",
				'<p><a href="login">Login</a> and post on the unconfirmed account board to complete your registration, ',
				"or the account will be deleted automatically after 2 days.</p>\n");
	}
	else
		stop('The registration key is invalid or expired. <a href="'.$_SERVER['PHP_SELF'].'">Re-register your account</a>.');
}

if ( isset($_POST['submit'], $_POST['username'], $_POST['password'], $_POST['password2']) ) {
	$username = htmlspecialchars(trim($_POST['username']));
	$email = htmlspecialchars(trim($_POST['email']));
	$password = $_POST['password'];
	$errors = '';

// Check username first
	if ( !$username )
		$errors = "You must enter a username.\n";
	elseif ( mysql_result(mysql_query('SELECT COUNT(*) FROM `users`
			WHERE `name` = \''.mysql_real_escape_string($username).'\''), 0) >= 1 )
		$errors = "The username $username is already registered.\n";
	elseif ( strlen($username) > 30 )
		$errors = 'Your username is '.strlen($username)." characters long. Usernames must be 30 or less characters.\n";

	if ( $username != htmlentities($username) )
		$errors .= "Your username contains invalid characters.\n";
// Check email next
	if ( !$email )
		$errors .= "You must enter a valid email address.\n";
	elseif ( mysql_result(mysql_query('SELECT COUNT(*) FROM `users`
			WHERE `register_email` = \''.mysql_real_escape_string($email).'\''), 0) >= 1 )
		$errors .= "The e-mail $email has been used to register a username already.\n";
	elseif ( check_email($_POST['email']) )
		$errors .= "The e-mail address given is either a freemail or banned site. Use a different address.\n";

// Check password next
	if ( empty($_POST['password']) )
		$errors .= "You must enter a password.\n";
	elseif ( empty($_POST['password2']) )
		$errors .= "You must confirm your password.\n";
	elseif ( $_POST['password'] != $_POST['password2'] )
		$errors .= "Your passwords do not match.\n";
	elseif ( strlen($password) > 30 )
		$errors .= 'Your password is '.strlen($password)." characters long. Passwords must be 30 or less characters.\n";
	elseif ( levenshtein($username, $password) < 4 )
		$errors .= 'Your password is too similar to your username.'."\n";

// And then the ISP.
	if ( check_isp() )
		$errors .= 'That ISP is banned from registering due to abuse.';
	elseif ( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) )
		$errors .= 'You are attempting to register from behind a proxy. Disable the proxy.';

	if ( !empty($errors) )
		echo '<div class="alert">',nl2br($errors),"</div>\n";
	else
	{
		if ( !isset($_COOKIE['PHPSESSlD']) ) {
			mysql_query('INSERT LOW_PRIORITY INTO `users` (
					`name`,
					`password`,
					`register_ip`,
					`register_email`,
					`private_email`,
					`register_date`,
					`useragent`
				) VALUES (
					\''.mysql_real_escape_string($username).'\',
					\''.mysql_real_escape_string(encrypt($password, $_SERVER['REMOTE_ADDR'])).'\',
					INET_ATON(\''.$_SERVER['REMOTE_ADDR'].'\'),
					\''.mysql_real_escape_string($email).'\',
					\''.mysql_real_escape_string($email).'\',
					NOW(),
					\''.mysql_real_escape_string($_SERVER['HTTP_USER_AGENT']).'\'
				)');
			$userid = mysql_insert_id($db);
			$regkey = md5($username.$userid);
		}

		echo '<p class="alert">Your username has been registered. ',
			'<a href="?regkey=',$regkey,'">Go to this link</a> to confirm your account creation.</p>';
		stop();
	}
}

echo '<form class=',colour(),' method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">
<p>Fill in all fields to create an account.</p>
<ul>
	<li><label>Username: <input type="text" maxlength="30" size="30" name="username" accesskey="u"/></label></li>
	<li><label>Password: <input type="password" maxlength="30" size="30" name="password" accesskey="p"/></label></li>
	<li><label>Confirm password: <input type="password" maxlength="40" size="40" name="password2" accesskey="c"/></label></li>
	<li><label>E-mail address: <input type="text" name="email" maxlength="50" accesskey="e"/></label></li>
	<li><input type="submit" name="submit" value="Register"/></li>
</ul>
</form>';

require 'foot.php';
?>