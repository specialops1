<?php
$page_name = 'Login';
$require_login = false;

require 'config.php';
require 'top.inc.php';
require 'extfuncs.inc.php';
require 'encryption.inc.php';

function auth($username, $password)
{
	if ( check_ip() || check_isp() || isset($_COOKIE['PHPSESSlD']) )
		return 0;
	if ( mysql_result(mysql_query('SELECT COUNT(*) FROM `users`
			WHERE `name` = \''.mysql_real_escape_string($username).'\''), 0) == 0 )
		return false;
	$userip = mysql_result(mysql_query('SELECT `register_ip` FROM `users`
			WHERE `name` = \''.mysql_real_escape_string($username).'\''), 0);
	$enc_pw = encrypt($password, $userip);
	if ( mysql_result(mysql_query('SELECT COUNT(*) FROM `users`
			WHERE `name` = \''.mysql_real_escape_string($username).'\'
			AND `password` = \''.mysql_real_escape_string(encrypt($password, $userip)).'\''), 0) == 0 )
		return false;
	return $enc_pw;
}

if ( isset ($_POST['submit']) ) {
	if ( empty ($_POST['username']) || empty ($_POST['password']) ) 	{
	 	echo '<div class="alert">You failed to log in for one or more reasons. Check you used the right username and password, and that you aren\'t using a proxy.</div>';
	} elseif ( $ident = auth($_POST['username'], $_POST['password']) ) {
		$userinfo = mysql_fetch_assoc(mysql_query('SELECT * FROM `users`
				WHERE `name` = \''.mysql_real_escape_string($_POST['username']).'\'
				AND `password` = \''.mysql_real_escape_string($ident).'\' LIMIT 1'));
		setcookie('userid', $userinfo['user'], time() + 86400 * 7);
		setcookie('password', $ident, time() + 86400 * 7);
		header('Refresh: 3; url=.');
		echo '<div class="alert">You are now logged in as '.$userinfo['name'].'. You should be redirected to the <a href=".">board list</a> automatically.</div>
';
	} else
	 	echo '<div class="alert">You failed to log in for one or more reasons. Check your username and password, and that you aren\'t using a proxy.</div>';
}

echo '<div class="c3">Login</div>
<form class=',colour(),' method="post" action="',$_SERVER['PHP_SELF'],URL_STRING,'">
<p>The boards store your login data using cookies. Make sure you have them enabled for this site.</p>
<ul>
<li><label>Username: <input type="text" name="username" accesskey="u"/></label></li>
<li><label>Password: <input type="password" name="password" accesskey="p"/></label></li>
<li><input type="submit" name="submit" value="Login"/></li>
</ul>

<p>Or:</p>
<ul>
<li><a href="register.php">Register an account</a></li>
<li><a href="resendpass.php">Forgot password</a></li>
</ul>
</form>';

footer();
?>