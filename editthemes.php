<?php
require ('config.php');
$level_restriction = ADMIN;
$require_login = true;
$page_name = 'Themes Editor';
require ('top.inc.php');

asort ($cfg['logotitles']);
function imglist ($imgsel = 0)
{
	global $cfg;
	$box = '<select name="imgsel">
';
	foreach ($cfg['logotitles'] as $index => $name)
		$box .= '<option value="'.$index.'"'.($imgsel == $index ? ' selected="selected"' : '').'>'.$name.'</option>
';
	return $box.'</select>
';
}

$themeid = isset ($_GET['tid']) ? intval ($_GET['tid']) : 'none';

if (isset ($_POST['submit']))
{
	if ($_POST['submit'] == 'Delete Theme')
	{
		mysql_query ('DELETE FROM `styles` WHERE `styleid` = '.intval ($_POST['styleid']).' LIMIT 1');
		echo '<div class="alert">Theme removed.</div>
';
	}
	elseif ($_POST['submit'] == 'Edit Theme')
	{
		mysql_query ('UPDATE `styles` SET
			`title` = \''.mysql_real_escape_string ($_POST['title']).'\',
			`filename` = \''.mysql_real_escape_string ($_POST['filename']).'\',
			`logo` = '.intval ($_POST['imgsel']).'
			WHERE `styleid` = '.intval ($_POST['styleid']).' LIMIT 1'
		);
		echo '<div class="alert">Theme modified.</div>
';
	}
	elseif ($_POST['submit'] == 'Add Theme')
	{
		mysql_query ('INSERT INTO `styles` (`title`, `filename`, `logo`) VALUES (
			\''.mysql_real_escape_string ($_POST['title']).'\',
			\''.mysql_real_escape_string ($_POST['filename']).'\',
			'.intval ($_POST['imgsel']).')'
		);
		echo '<div class="alert">Theme added.</div>
';
	}
	elseif ($_POST['submit'] == 'Change Default')
	{
		mysql_query ('UPDATE `styles` SET `defaulttheme` = 0');
		mysql_query ('UPDATE `styles` SET `defaulttheme` = 1 WHERE `styleid` = '.intval ($_POST['colorsel']).' LIMIT 1');
		echo '<div class="alert">Default theme modified.</div>
';
	}
}

echo '<div class="c3">Manage Premade Themes</div>
<table>
<tr><th>Name</th><th>Filename</th><th>Logo Image</th><th>Edit</th></tr>
';

$result = mysql_query ('SELECT * FROM `styles` ORDER BY `title` ASC');
while ($myrow = mysql_fetch_assoc ($result))
{
	if ($themeid != $myrow['styleid'])
     	echo '<tr class='.colour().'>
<td>'.$myrow['title'].' ('.mysql_result (mysql_query('SELECT COUNT(*) FROM `users` WHERE `colorsel` = '.$myrow['styleid'].' AND `themetype` = 0'), 0).' users)</td>
<td>'.$myrow['filename'].(file_exists ('csslib/system/'.$myrow['filename']) ? '' : $cfg['img'][3]).'</td>
<td>'.$cfg['logotitles'][$myrow['logo']].'</td>
<td><a href="?op=delete;tid='.$myrow['styleid'].urlpath(1).'">Delete</a><br />
<a href="?op=edit;tid='.$myrow['styleid'].urlpath(1).'">Edit</a></td>
</tr>
';
	elseif ($_GET['op'] == 'delete')
     	echo '<tr class="alert">
<td>'.$myrow['title'].'</td>
<td>'.$myrow['filename'].'</td>
<td>'.$cfg['logotitles'][$myrow['logo']].'</td>
<td><form action="'.urlpath(2).'" method="post">
<input type="hidden" name="styleid" value="'.$myrow['styleid'].'"/>
<input type="submit" name="submit" value="Delete Theme"/><br/>
<input type="submit" value="Cancel"/></form></td>
</tr>
';
	elseif ($_GET['op'] == 'edit')
     	echo '<tr class="alert"><td colspan="5">
<form action="'.urlpath(2).'" method="post">
<input type="hidden" name="styleid" value="'.$myrow['styleid'].'"/>
Title: <input type="text" name="title" value="'.$myrow['title'].'"/><br/>
Filepath: <input type="text" name="filename" value="'.$myrow['filename'].'"/><br/>
Header: '.imglist ($myrow['logo']).'<br/>
<input type="submit" name="submit" value="Edit Theme"/> | <input type="submit" value="Cancel"/>
</form>
</td></tr>
';
}

echo '</table>

<div class="c3">Add theme</div>
<form action="'.urlpath(2).'" method="post">
<div class="alert">
Title: <input type="text" name="title"/><br/>
Filename: <input type="text" name="filename"/> (base filename only)<br/>
Header: '.imglist().'<br/>
<input type="submit" name="submit" value="Add Theme"/>
</div>
</form>

<form action="'.urlpath(2).'" method="post">
<div class='.colour().'>
Default theme:
<select name="colorsel">
';

$themes = mysql_query ('SELECT `styleid`, `title`, `defaulttheme` FROM `styles` ORDER BY `title` ASC');
while ($style = mysql_fetch_row ($themes))
	echo '<option value="'.$style[0].'"'.($style[2] ? ' selected="selected"' : '').'>'.$style[1].'</option>
';

echo '</select>
<input type="submit" name="submit" value="Change Default"/>
</div>
</form>';

require ('foot.php');
?>