<?php
require 'config.php';
$require_login = true;
$page_name = 'Board Editor';
$level_restriction = ADMIN;
require 'top.inc.php';

require 'include/levels.php';
foreach ( array_keys($cfg['levels']) as $level ) {
	list($name) = explode(': ', $cfg['leveldesc'][$level]);
	$levels[$level] = $name;
}

function levlist($fieldname, $levelnum = INACTIVE_USER)
{
	global $cfg, $levels;

	$box = '<select name="'.$fieldname.'">';
	foreach ( $levels as $a => $b )
		$box .= '<option value="'.$a.( $levelnum == $a ? '" selected="selected' : '' ).'">'.$a.': '.$b."</option>\n";
	return $box."</select>\n";
}
?>

<?php
$tmp = mysql_query('SELECT * FROM `board-groups`');
while ( $g = mysql_fetch_assoc($tmp) ) {

	echo '<h3>Group ID ',implode(': ', $g),'</h3>
	<table><thead><tr><th>Board Name, ID, Caption</th><th>View Level</th><th>Post Level</th><th>Topic Level</th></tr></thead><tbody>';

	$tmp = mysql_query('SELECT * FROM `boards` WHERE `group` = '.$g['group']);
	while ( $b = mysql_fetch_assoc($tmp) ) {
		echo '<tr class=',colour(),">\n",
		'	<td>',$b['name'],' (',$b['board'],')<br/><small>',$b['caption'],"</small></td>\n",
		'	<td>',$b['view_level'],'</td><td>',$b['post_level'],'</td><td>',$b['topic_level'],"</td>\n",
		'</tr>';
	}

	echo '</tbody>
	</table>';

}

footer();
?>
