<?php
require 'config.php';

$require_login = true;

isset($_GET['list']) ? '' : $_GET['list'] = 'all';

switch ( $_GET['list'] ) {
	case 'online':
		$page_name = 'Online Users';
		$list_criteria = '`last_active` > (NOW() - 600)';
	break;
	case 'msie':
		$page_name = 'IE Users';
		$list_criteria = '`useragent` LIKE \'%MSIE%\'';
	break;
	case 'banned':
		$page_name = 'Banned / Suspended Users';
		$list_criteria = '`level` <= '.SUSPENDED_USER;
	break;
	default:
		$page_name = 'User Directory';
		$list_criteria = 1;
}

require 'top.inc.php';
require 'users.inc.php';

footer();
?>
