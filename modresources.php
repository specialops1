<?php
require 'config.php';
$require_login = true;
$page_name = 'Moderator Resources';
$level_restriction = MOD;
if ( isset($_GET['phpinfo']) && $userinfo['level'] >= ADMIN ) {
	phpinfo();
	exit;
}
require 'top.inc.php';

list($total_marks) = mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `marks` WHERE `actioned` IS NULL'));

echo '<h2>Links</h2>
<ul class=',colour(),'>
<li><a href="markqueue',URL_STRING,'">Message Queue: ',$total_marks,'</a></li>
<li><a href="sendsysnote',URL_STRING,'">Send sysnotes</a></li>
<li><a href="userdir',URL_STRING,'">User Directory</a></li>
<li>IP address info:
<form action="ipinfo',URL_STRING,'" method="get" style="display: inline"><input type="text" name="ip"/></form></li>
</ul>

<h2>Moderator Activity</h2>
<table>
<thead>
<tr>
	<th rowspan="2" scope="col">Moderator</th>
	<th rowspan="2" scope="col">Marks Handled</th>
	<th colspan="2">Suggestions Handled</th>
</tr>
<tr>
	<th scope="col">Positive</th>
	<th scope="col">Negative</th>
</tr>
</thead>
<tbody>
';

$tmp = mysql_query('SELECT `user` FROM `users` WHERE `level` >= '.MOD);
while ( list($m) = mysql_fetch_row($tmp) ) {
	list($na_marks) = mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `marks` WHERE `action` = 0 AND `moderator` = '.$m));
	list($p_marks) = mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `marks` WHERE `action` > 0 AND `moderator` = '.$m));
	list($n_marks) = mysql_fetch_row(mysql_query('SELECT COUNT(*) FROM `marks` WHERE `action` < 0 AND `moderator` = '.$m));
	echo '<tr class=',colour(),'><td>',userlink($m),'</td><td>',$na_marks,'</td><td>',$p_marks,'</td><td>',$n_marks,"</td></tr>\n";
}
echo "</tbody>\n</table>\n";

if ( $userinfo['level'] < ADMIN )
	stop();

if ( isset($_GET['purge']) ) {
	define('FORCE_PURGE', true);
	include 'thepurge.php';
	echo '<div class="alert">Purge ran.</div>'."\n";
}

if ( isset($_GET['activate']) ) {
	$tmp = mysql_query('SELECT `user` FROM `users` WHERE `level` = '.INACTIVE_USER);
	while (list($u) = mysql_fetch_row($tmp))
		if ( mysql_result(mysql_query('SELECT COUNT(*) FROM `messages` WHERE `user` = '.$u), 0) )
			mysql_query('UPDATE `users` SET `level` = '.NEW_USER.' WHERE `user` = '.$u);
	echo '<div class="alert">Users updated.</div>',"\n";
}

echo '<h2>Admin Resources</h2>

<h3>Editor stuff</h3>
<ul class="c1">
<li><a href="editboard',URL_STRING,'">Board Editor</a></li>
',( $userinfo['level'] == ADMIN2 ? '<li><a href="editlevels'.URL_STRING.'">Level Editor</a></li>' : '' ),'
<li><a href="editthemes',URL_STRING,'">Themes Editor</a></li>
<li><a href="editpoll',URL_STRING,'">Poll Editor</a></li>
</ul>

<h3>Inactive, suspended and banned users</h3>';

$list_criteria = '`level` <= '.INACTIVE_USER;
require 'users.inc.php';

echo '<h3>Other</h3>
<ul class="c1">
<li><a href="',$_SERVER['PHP_SELF'],'?purge=1',URL_APPEND,'">Manual Purge</a></li>
<li><a href="',$_SERVER['PHP_SELF'],'?activate=1',URL_APPEND,'">Activate all inactive users</a></li>
',( $userinfo['level'] == ADMIN2 ? '<li><a href="'.$_SERVER['PHP_SELF'].'?phpinfo=1">phpinfo()</a></li>' : '' ),'
<li>Image check: ',$cfg['img'][0],$cfg['img'][1],$cfg['img'][2],$cfg['img'][3],'</li>
</ul>';

footer();
?>
