<?php
if (mysql_result (mysql_query ('SELECT `lastpurge` FROM `misc2`'), 0) != gmdate ('Y-m-d') || defined ('FORCE_PURGE'))
{
	// user purge
	$result = mysql_query ('SELECT `cookies`, `level`, `userid`, `lastsec` FROM `users` ORDER BY `userid` ASC');
	while ($myrow = mysql_fetch_row ($result))
	{
		switch ($myrow[1])
		{
			case PENDING_EMAIL: case INACTIVE_USER: $timeout = 86400; break;	//1 day
			case SUSPENDED_USER: $timeout = 7776000; break;	//90 days
			case KOS: case BANNED_USER: $timeout = 86400*120; break;
			default: $timeout = 31536000; //year
		}
		if (($myrow[3] < time() - $timeout) && mysql_result (mysql_query ('SELECT COUNT(*) FROM `msgmeta` WHERE `postedby` = '.$myrow[2]), 0) == 0)
			mysql_query ('DELETE FROM `users` WHERE `userid` = '.$myrow[2].' LIMIT 1');
		elseif (!mysql_result (mysql_query ('SELECT `nochange` FROM `levels` WHERE `levnum` = '.$myrow[1]), 0) && $myrow[0] >= 0) // in other word, if the level is changeable
		{
			$new_level = mysql_result (mysql_query ('SELECT `levnum` FROM `levels` WHERE `minaura` <= '.intval ($myrow[0]).' AND `nochange` = 0 ORDER BY `minaura` DESC LIMIT 1'), 0);
			if ($new_level >= NEW_USER)
				mysql_query ('UPDATE LOW_PRIORITY `users` SET `level` = '.intval ($new_level).' WHERE `userid` = '.$myrow[2]);
		}
	}

// Message purge
	/* kill topics with no posts */
	mysql_query ('DELETE FROM `topics` WHERE
		(`topicid` NOT IN(SELECT `topic` FROM `msgmeta`)) OR
		(`boardnum` NOT IN(SELECT `boardid` FROM `boards`))'
	);
	/* kill posts with no topic */
	mysql_query ('DELETE FROM `msgmeta` WHERE
		(`topic` NOT IN(SELECT `topicid` FROM `topics`)) OR
		(`mesboard` NOT IN(SELECT `boardid` FROM `boards`)) OR
		(`msgid` NOT IN(SELECT `messageid` FROM `messages`))'
	);
	mysql_query ('DELETE FROM `messages` WHERE `messageid` NOT IN(SELECT `msgid` FROM `msgmeta`)');
	mysql_query ('OPTIMIZE TABLE `topics`, `messages`');

	/* system cleanup */
	mysql_query ('DELETE FROM `iplog` WHERE
		(`actiontype` IN(\'message\', \'topic\')) OR
		(`actiontype` IN(\'register\', \'resendpass\') AND `logsec` < (UNIX_TIMESTAMP() - 172800)) OR
		(`actiontype` IN(\'login\', \'edituser\') AND `logsec` < (UNIX_TIMESTAMP() - 2592000))'
	);

	/* vim rape */
	mysql_query ('UPDATE `users` SET `points` = (`points` - 1) WHERE `points` > -20 AND `level` >= '.NEW_USER.' AND `lastsec` >= (UNIX_TIMESTAMP() - 86400)');

	/* closed accounts */
	mysql_query ('UPDATE `users` SET `level` = '.CLOSED.' WHERE `level` = '.PENDING_CLOSE.' AND `lastsec` < (UNIX_TIMESTAMP() - 86400)');

	/* activity thinger */
	$myrow = mysql_result (mysql_query ('SELECT `mostactive` FROM `misc2`'), 0);
	$myrow2 = mysql_result (mysql_query ('SELECT COUNT(*) FROM `msgmeta` WHERE `messsec` >= (UNIX_TIMESTAMP() - 86400)'), 0);
	if ($myrow2 > $myrow)
		mysql_query ('UPDATE `misc2` SET `mostactiveat` = '.(time() - 86400).', `mostactive` = '.$myrow2.'');

	/* update purge run time */
	mysql_query ('UPDATE `misc2` SET `lastpurge` = \''.gmdate ('Ymd').'\'');
	mysql_query ('OPTIMIZE TABLE `users`, `iplog`, `systemnot`');
}
?>
