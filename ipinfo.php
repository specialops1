<?php
require ('config.php');
$level_restriction = MOD;
$require_login = true;

$ipaddr = isset ($_GET['ip']) ? mysql_real_escape_string ($_GET['ip']) : $userinfo['lastacip'];
$page_name = 'Stats for '.$ipaddr;
require ('top.inc.php');

echo '<form action="'.urlpath(2).'" method="get" style="display: inline">
<div class="alert">IP address: <input type="text" name="ip" value="'.$ipaddr.'"/></div>
</form>

<div class="c3">Usermap by last user IPs</div>';
$list_criteria = '`lastacip` = \''.$ipaddr.'\'';
require ('users.inc.php');

echo '<div class="c3">Usermap by iplog table</div>';
$list_criteria = '`userid` IN (SELECT `userid` FROM `iplog` WHERE `remoteip` = \''.$ipaddr.'\')';
require ('users.inc.php');

$stats[] = mysql_result (mysql_query ('SELECT COUNT(*) FROM `iplog` WHERE `actiontype` = \'login\' AND `remoteip` = \''.$ipaddr.'\''), 0);
$stats[] = mysql_result (mysql_query ('SELECT COUNT(*) FROM `iplog` WHERE `actiontype` = \'oklogin\' AND `remoteip` = \''.$ipaddr.'\''), 0);
$stats[] = mysql_result (mysql_query ('SELECT COUNT(*) FROM `iplog` WHERE `actiontype` = \'message\' AND `remoteip` = \''.$ipaddr.'\''), 0);
$stats[] = mysql_result (mysql_query ('SELECT COUNT(*) FROM `iplog` WHERE `actiontype` = \'topic\' AND `remoteip` = \''.$ipaddr.'\''), 0);
$stats[] = mysql_result (mysql_query ('SELECT COUNT(*) FROM `iplog` WHERE `actiontype` = \'resendpass\' AND `remoteip` = \''.$ipaddr.'\''), 0);
$stats[] = mysql_result (mysql_query ('SELECT COUNT(*) FROM `iplog` WHERE `actiontype` = \'ipban\' AND `remoteip` = \''.$ipaddr.'\''), 0);
echo '<div class="c3">Logged actions from this IP address</div>
<table>
<tr class='.colour().'><td>Bad Logins</td><td>'.$stats[0].'</td></tr>
<tr class='.colour().'><td>Logins</td><td>'.$stats[1].'</td></tr>
<tr class='.colour().'><td>Posts</td><td>'.$stats[2].'</td></tr>
<tr class='.colour().'><td>Topics</td><td>'.$stats[3].'</td></tr>
<tr class='.colour().'><td>Resendpass</td><td>'.$stats[4].'</td></tr>
<tr class='.colour().'><td>Hostname</td><td>'.gethostbyaddr ($ipaddr).'</td></tr>
<tr class='.colour().'><td>Is IP-banned</td><td>'.($stats[5] ? 'Yes' : 'No').'</td></tr>
</table>

<div class="c3">Post history from this IP</div>';
$list_criteria = 'postedip = \''.$ipaddr.'\' ORDER BY `messsec` DESC';
$show_titles = true;
require ('messages.inc.php');

require ('foot.php');
?>